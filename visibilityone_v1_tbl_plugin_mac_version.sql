-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_plugin_mac_version`
--

DROP TABLE IF EXISTS `tbl_plugin_mac_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_plugin_mac_version` (
  `plugin_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`plugin_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_plugin_mac_version`
--

LOCK TABLES `tbl_plugin_mac_version` WRITE;
/*!40000 ALTER TABLE `tbl_plugin_mac_version` DISABLE KEYS */;
INSERT INTO `tbl_plugin_mac_version` VALUES (1,'1.0.0','VisibilityOnePluginSetup-1.0.0.pkg','2022-03-31 15:21:50',0,'256MB','Phase 1','For release',NULL,NULL,1),(2,'1.0.1','VisibilityOnePluginSetup-1.0.1.pkg','2022-06-05 15:21:50',0,'256MB','Phase 1','For release',NULL,NULL,1),(3,'1.0.2','VisibilityOnePluginSetup-1.0.2.pkg','2022-06-06 15:21:50',0,'256MB','Phase 1','For release',NULL,NULL,1),(4,'1.0.5','VisibilityOnePluginSetup-1.0.5.pkg','2022-07-05 15:21:50',0,'256MB','Phase 1','For release',NULL,NULL,1),(5,'1.0.6','VisibilityOnePluginSetup-1.0.6.pkg','2022-07-07 15:21:50',0,'256MB','Phase 1','For release',NULL,NULL,1),(6,'1.0.7','VisibilityOnePluginSetup-1.0.7.pkg','2022-07-07 15:21:50',0,'256MB','Phase 1','For release',NULL,NULL,1),(7,'1.0.8','VisibilityOnePluginSetup-1.0.8.pkg','2022-08-17 15:21:50',0,'256MB','Font fix','For release',NULL,NULL,1),(8,'1.0.9','VisibilityOnePluginSetup-1.0.9.pkg','2022-08-17 15:21:50',0,'99.4MB','Extended Logging','For release',NULL,NULL,1),(9,'1.0.10','VisibilityOnePluginSetup-1.0.10.pkg','2022-08-27 15:21:50',0,'99.4MB','Additional fix','For release',NULL,NULL,1),(10,'1.1.0','VisibilityOnePluginSetup-1.1.0.pkg','2022-08-30 15:21:50',1,'99.4MB','Host info fix','For release',NULL,NULL,1);
/*!40000 ALTER TABLE `tbl_plugin_mac_version` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:24:03
