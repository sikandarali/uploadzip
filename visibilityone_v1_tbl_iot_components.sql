-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_iot_components`
--

DROP TABLE IF EXISTS `tbl_iot_components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_iot_components` (
  `iot_id` bigint NOT NULL AUTO_INCREMENT,
  `quadrant_group` enum('VIDEO','AUDIO','CLOUD') DEFAULT NULL,
  `device_id` bigint DEFAULT NULL,
  `collector_id` bigint DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `iot_type` enum('DISPLAY','SERVER','NETWORK','ACCESS POINT','POWER','WEB','CAMERA','DEVICE','CLOUD') DEFAULT NULL,
  `iot_name` varchar(100) DEFAULT NULL,
  `iot_ip` varchar(100) DEFAULT NULL,
  `iot_url_status` tinyint(1) NOT NULL DEFAULT '0',
  `iot_url` varchar(150) DEFAULT NULL,
  `host_name` varchar(127) DEFAULT NULL,
  `make` varchar(127) DEFAULT NULL,
  `mac_address` varchar(50) DEFAULT NULL,
  `other_data` json DEFAULT NULL,
  `is_monitored` tinyint DEFAULT '0',
  `company_id` int unsigned DEFAULT NULL,
  `health_trigger_points` json DEFAULT NULL,
  `health` int unsigned DEFAULT '0',
  `notes` longtext,
  `site_id` int unsigned DEFAULT NULL,
  `pause` tinyint DEFAULT '0',
  PRIMARY KEY (`iot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_iot_components`
--

LOCK TABLES `tbl_iot_components` WRITE;
/*!40000 ALTER TABLE `tbl_iot_components` DISABLE KEYS */;
INSERT INTO `tbl_iot_components` VALUES (1,NULL,NULL,675,1,0,'CLOUD','vs pro','172.19.3.117',0,NULL,'Polycom64167f7807f2','Polycom','64:16:7F:78:07:F2','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http_alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom_tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,717,0),(2,NULL,NULL,675,1,0,'WEB','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,717,0),(3,NULL,NULL,675,1,0,'DEVICE','poly VISUAL+','172.19.3.117',0,NULL,'host','Polycom','64:16:7F:78:07:F2','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http-alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom-tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,717,0),(4,NULL,NULL,675,1,0,'POWER','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,717,0),(5,NULL,NULL,675,1,0,'POWER','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,717,0),(6,NULL,NULL,672,1,0,'SERVER','Google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,718,0),(7,NULL,NULL,672,1,0,'SERVER','Google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,718,0),(8,NULL,NULL,675,1,0,'DEVICE','testroom VISUAL+','172.19.3.124',0,NULL,'host','Polycom','64:16:7F:2E:2F:0C','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http-alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom-tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,717,0),(9,'AUDIO',482,675,1,0,'DISPLAY','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,717,0),(10,NULL,NULL,674,1,0,'DEVICE','a audio VISUAL+','172.19.3.117',0,NULL,'host','Polycom','64:16:7F:78:07:F2','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http-alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom-tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,720,1),(11,NULL,NULL,674,1,0,'DEVICE','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,720,1),(12,NULL,NULL,674,1,0,'DEVICE','a audio VISUAL+','172.19.3.124',0,NULL,'host','Polycom','64:16:7F:2E:2F:0C','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http-alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom-tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,720,0),(13,NULL,NULL,678,1,0,'NETWORK','Test','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,723,0),(14,NULL,NULL,674,1,0,'ACCESS POINT','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,720,0),(15,NULL,NULL,668,1,0,'NETWORK','Poly','172.19.3.110',0,NULL,'\"null\"','ViaVideo Communications','00:E0:DB:53:C9:84','[]',1,36,NULL,0,NULL,701,0),(16,NULL,NULL,676,1,0,'DISPLAY','test','172.19.3.124',0,NULL,'Polycom64167f2e2f0c','Polycom','64:16:7F:2E:2F:0C','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http_alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom_tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,0,NULL,721,0),(17,NULL,NULL,679,1,0,'DISPLAY','new name','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,725,0),(18,NULL,NULL,679,1,0,'DISPLAY','Today testing','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,725,0),(19,NULL,NULL,674,1,0,'DEVICE','a audio VISUAL+','172.19.3.124',0,NULL,'host','Polycom','64:16:7F:2E:2F:0C','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http-alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom-tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,100,'',720,0),(20,NULL,NULL,676,1,1,'NETWORK','Test Dev','172.19.3.52',0,NULL,'null','ViaVideo Communications','00:E0:DB:41:CC:4C','[{\"portid\": \"22\", \"portname\": \"ssh\", \"is_monitored\": 1}, {\"portid\": \"23\", \"portname\": \"telnet\", \"is_monitored\": 1}, {\"portid\": \"24\", \"portname\": \"priv_mail\", \"is_monitored\": 1}, {\"portid\": \"80\", \"portname\": \"http\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"1720\", \"portname\": \"h323q931\", \"is_monitored\": 1}, {\"portid\": \"5001\", \"portname\": \"commplex_link\", \"is_monitored\": 1}, {\"portid\": \"5061\", \"portname\": \"sip_tls\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,721,0),(21,NULL,NULL,676,1,0,'SERVER','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,0,NULL,721,1),(22,NULL,NULL,676,1,0,'DEVICE','test audio VISUAL+','172.19.3.124',0,NULL,'host','Polycom','64:16:7F:2E:2F:0C','[{\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}, {\"portid\": \"8000\", \"portname\": \"http-alt\", \"is_monitored\": 1}, {\"portid\": \"8001\", \"portname\": \"vcom-tunnel\", \"is_monitored\": 1}, {\"portid\": \"10010\", \"portname\": \"rxapi\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,721,0),(23,NULL,NULL,676,1,1,'NETWORK','google','8.8.8.8',0,NULL,'dns.google','Unknown','null','[{\"portid\": \"53\", \"portname\": \"domain\", \"is_monitored\": 1}, {\"portid\": \"443\", \"portname\": \"https\", \"is_monitored\": 1}]',1,36,NULL,100,NULL,733,0);
/*!40000 ALTER TABLE `tbl_iot_components` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:51
