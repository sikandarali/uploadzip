-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_zoom_user_live_qos`
--

DROP TABLE IF EXISTS `tbl_zoom_user_live_qos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_zoom_user_live_qos` (
  `zoom_user_live_qos_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `zoom_user_id` int unsigned DEFAULT NULL,
  `company_id` int unsigned DEFAULT NULL,
  `id` varchar(45) DEFAULT NULL,
  `user_name` varchar(45) DEFAULT NULL,
  `meetingId` bigint DEFAULT NULL,
  `meeting_uuid` varchar(127) DEFAULT NULL,
  `device` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `camera` varchar(255) DEFAULT NULL,
  `speaker` varchar(255) DEFAULT NULL,
  `microphone` varchar(255) DEFAULT NULL,
  `network_type` varchar(45) DEFAULT NULL,
  `connection_type` varchar(45) DEFAULT NULL,
  `recording` int DEFAULT NULL,
  `stamp` datetime DEFAULT NULL,
  `date_time` datetime DEFAULT NULL,
  `total_bitrate` bigint DEFAULT NULL,
  `total_latency` int DEFAULT NULL,
  `total_jitter` int DEFAULT NULL,
  `avg_packet_loss` double DEFAULT NULL,
  `audio_input` json DEFAULT NULL,
  `audio_output` json DEFAULT NULL,
  `video_input` json DEFAULT NULL,
  `video_output` json DEFAULT NULL,
  `as_input` json DEFAULT NULL,
  `as_output` json DEFAULT NULL,
  `frame_rate` int DEFAULT NULL,
  `health` int DEFAULT NULL,
  `health_info` json DEFAULT NULL,
  `qos` int DEFAULT NULL,
  `people_count` int DEFAULT '0',
  PRIMARY KEY (`zoom_user_live_qos_id`)
) ENGINE=InnoDB AUTO_INCREMENT=123048 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_zoom_user_live_qos`
--

LOCK TABLES `tbl_zoom_user_live_qos` WRITE;
/*!40000 ALTER TABLE `tbl_zoom_user_live_qos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_zoom_user_live_qos` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:24:23
