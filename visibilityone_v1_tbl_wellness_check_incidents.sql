-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_wellness_check_incidents`
--

DROP TABLE IF EXISTS `tbl_wellness_check_incidents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_wellness_check_incidents` (
  `wellness_check_id` bigint NOT NULL AUTO_INCREMENT,
  `company_id` bigint DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `duration` int DEFAULT NULL,
  `site_id` int DEFAULT NULL,
  `active` int DEFAULT NULL,
  `notification_info` json DEFAULT NULL,
  `notification_created` int DEFAULT '0',
  `quadrant_group` enum('VIDEO','AUDIO','CLOUD','DOMOTZ_IOT') DEFAULT NULL,
  `device_id` bigint DEFAULT NULL,
  `events` json DEFAULT NULL,
  `wakeup_status` varchar(50) DEFAULT NULL,
  `reboot_status` varchar(50) DEFAULT NULL,
  `camera_status` varchar(50) DEFAULT NULL,
  `exchange_status` varchar(50) DEFAULT NULL,
  `mic_status` varchar(50) DEFAULT NULL,
  `gatekeeper_status` varchar(50) DEFAULT NULL,
  `video_incident_id` bigint DEFAULT NULL,
  `log_status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`wellness_check_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_wellness_check_incidents`
--

LOCK TABLES `tbl_wellness_check_incidents` WRITE;
/*!40000 ALTER TABLE `tbl_wellness_check_incidents` DISABLE KEYS */;
INSERT INTO `tbl_wellness_check_incidents` VALUES (1,36,'2023-09-03 04:15:10','2023-09-03 06:27:50',NULL,NULL,0,'{\"message\": \"Video device has some issues - - Collector can not communicate with the device via API’s Download log failed . \"}',1,'VIDEO',7047,NULL,'COMPLETE','COMPLETE','SUCCESS','ERROR','SUCCESS','ERROR',NULL,'FAILED'),(2,36,'2023-09-03 04:15:10','2023-09-03 06:27:27',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE'),(3,36,'2023-09-04 14:15:27','2023-09-04 14:18:40',NULL,NULL,0,'{\"message\": \"Video device has some issues - - Collector can not communicate with the device via API’s Download log failed . \"}',1,'VIDEO',7047,NULL,'COMPLETE','COMPLETE','SUCCESS','ERROR','SUCCESS','ERROR',NULL,'FAILED'),(4,36,'2023-09-04 14:15:27','2023-09-04 14:18:16',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE'),(5,36,'2023-09-08 08:30:13','2023-09-08 08:30:13',NULL,NULL,0,'{\"message\": \"Collector fail when initiating wellness\"}',1,'VIDEO',7047,NULL,'ERROR','ERROR','UNDETECTED','ERROR','UNDETECTED','UNDETECTED',28551,'ERROR'),(6,36,'2023-09-08 14:30:18','2023-09-08 14:33:04',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE'),(7,36,'2023-09-09 08:30:02','2023-09-09 08:30:02',NULL,NULL,0,'{\"message\": \"Collector fail when initiating wellness\"}',1,'VIDEO',7047,NULL,'ERROR','ERROR','UNDETECTED','ERROR','UNDETECTED','UNDETECTED',28551,'ERROR'),(8,36,'2023-09-09 14:30:04','2023-09-09 14:32:53',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE'),(9,36,'2023-09-10 08:30:25','2023-09-10 08:30:25',NULL,NULL,0,'{\"message\": \"Collector fail when initiating wellness\"}',1,'VIDEO',7047,NULL,'ERROR','ERROR','UNDETECTED','ERROR','UNDETECTED','UNDETECTED',28551,'ERROR'),(10,36,'2023-09-10 14:30:01','2023-09-10 14:32:27',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE'),(11,36,'2023-09-11 08:30:06','2023-09-11 08:30:06',NULL,NULL,0,'{\"message\": \"Collector fail when initiating wellness\"}',1,'VIDEO',7047,NULL,'ERROR','ERROR','UNDETECTED','ERROR','UNDETECTED','UNDETECTED',28551,'ERROR'),(12,36,'2023-09-11 14:30:17','2023-09-11 14:33:01',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE'),(13,36,'2023-09-12 08:30:01','2023-09-12 08:30:01',NULL,NULL,0,'{\"message\": \"Collector fail when initiating wellness\"}',1,'VIDEO',7047,NULL,'ERROR','ERROR','UNDETECTED','ERROR','UNDETECTED','UNDETECTED',28551,'ERROR'),(14,36,'2023-09-12 14:30:21','2023-09-12 14:32:48',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE'),(15,36,'2023-09-13 08:30:06','2023-09-13 08:30:06',NULL,NULL,0,'{\"message\": \"Collector fail when initiating wellness\"}',1,'VIDEO',7047,NULL,'ERROR','ERROR','UNDETECTED','ERROR','UNDETECTED','UNDETECTED',28551,'ERROR'),(16,36,'2023-09-13 14:30:26','2023-09-13 14:33:12',NULL,NULL,0,'{\"message\": \"Video device has some issues -\"}',1,'VIDEO',7046,NULL,'COMPLETE','COMPLETE','SUCCESS','SUCCESS','SUCCESS','ERROR',NULL,'COMPLETE');
/*!40000 ALTER TABLE `tbl_wellness_check_incidents` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:24:32
