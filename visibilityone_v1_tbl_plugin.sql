-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_plugin`
--

DROP TABLE IF EXISTS `tbl_plugin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_plugin` (
  `plugin_id` int unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int unsigned DEFAULT NULL,
  `company_id` int unsigned DEFAULT NULL,
  `mac_address` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `host_name` varchar(127) DEFAULT NULL,
  `room_name` varchar(127) DEFAULT NULL,
  `network_type` varchar(45) DEFAULT NULL,
  `pin` varchar(20) DEFAULT NULL,
  `active` int DEFAULT '0',
  `status` int DEFAULT '-1',
  `created_date` datetime DEFAULT NULL,
  `verified` int DEFAULT '0',
  `cpu_info` json DEFAULT NULL,
  `host_info` json DEFAULT NULL,
  `network_adapters` json DEFAULT NULL,
  `disk_info` json DEFAULT NULL,
  `memo_info` json DEFAULT NULL,
  `application` json DEFAULT NULL,
  `version` varchar(45) DEFAULT NULL,
  `verify_date` datetime DEFAULT NULL,
  `last_connect` datetime DEFAULT NULL,
  `ping_period` int DEFAULT '10',
  `get_action_list_period` int DEFAULT '10',
  `data_send_period` int DEFAULT '10',
  `plugin_update_schedule` datetime DEFAULT NULL,
  `scheduled_update_setting` tinyint DEFAULT '0',
  `teams_application` json DEFAULT NULL,
  PRIMARY KEY (`plugin_id`)
) ENGINE=InnoDB AUTO_INCREMENT=490 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_plugin`
--

LOCK TABLES `tbl_plugin` WRITE;
/*!40000 ALTER TABLE `tbl_plugin` DISABLE KEYS */;
INSERT INTO `tbl_plugin` VALUES (481,NULL,36,'00:1F:C6:9D:01:F5',NULL,NULL,NULL,'Ethernet 802.3','4lco2y9',1,-1,'2023-03-17 06:19:32',1,'[{\"cpu\": 8, \"cpu_total\": 42, \"cpu_v1plugin\": 0, \"cpu_v1collector\": 0}]','[{\"mac\": \"00:1F:C6:9D:01:F5\", \"os_sn\": \"00331-20300-00000-AA354\", \"domain\": \"WORKGROUP\", \"bios_sn\": \"                                 \", \"hostname\": \"DESKTOP-3KFSIAC\", \"timezone\": \"-07:00:00\", \"utc_time\": \"2023-07-13T04:26:36.2724777Z\", \"user_name\": \"DESKTOP-3KFSIAC\\\\PanCast-Sales5\", \"wifi_ssid\": \"\", \"ip_address\": \"172.19.3.119\", \"win_os_ver\": \"10.0.19045\", \"last_reboot\": \"2023-07-11T18:59:36.5Z\", \"master_volume\": 3, \"wifi_signal_strength\": \"\"}]','[{\"dns1\": \"172.19.3.254\", \"dns2\": \"\", \"dns3\": \"\", \"nic_name\": \"Intel(R) Ethernet Connection (2) I219-LM\", \"ipv4_addr\": \"172.19.3.119\", \"ipv6_addr\": \"fe80::c559:15fc:268e:a26f\", \"mac_address\": \"00:1F:C6:9D:01:F5\"}]','[{\"free\": 151699726336, \"drive\": \"C:\", \"total\": 248499388416}]','[{\"mem_free\": 12859846656, \"mem_total\": 17063542784, \"mem_v1plugin\": 90599424, \"mem_v1collector\": 0}]','[{\"zaapi\": -1, \"panacast\": -1, \"team_room\": -1, \"zoom_room\": -1, \"v1collector\": -1, \"logitech_sync\": 1}, {\"v1plugin_failover\": false, \"v1plugin_selfhealing\": false, \"v1plugin_tracefeature\": false}]','2.0.0.6','2023-03-17 06:19:32','2023-07-13 04:26:38',10,10,30,NULL,0,'{\"user\": null, \"call_status\": true}'),(482,NULL,36,'58:96:1D:12:5D:B8',NULL,NULL,'Poly - Lenovo','Ethernet 802.3','00qfj75',1,1,'2023-05-03 03:06:58',1,'[{\"cpu\": 4, \"cpu_total\": 0, \"cpu_v1plugin\": 0, \"cpu_v1collector\": 0}]','[{\"mac\": \"6C:4B:90:FC:CF:22\", \"os_sn\": \"00436-20000-72521-AAOEM\", \"domain\": \"WORKGROUP\", \"bios_sn\": \"YH011K1L\", \"hostname\": \"DESKTOP-C923D39\", \"timezone\": \"-07:00:00\", \"utc_time\": \"2023-09-07T06:59:04.4081006Z\", \"user_name\": \"DESKTOP-C923D39\\\\ZoomRooms\", \"wifi_ssid\": \"\", \"ip_address\": \"172.19.3.137\", \"win_os_ver\": \"10.0.19045\", \"last_reboot\": \"2023-09-02T10:48:42.5Z\", \"master_volume\": 0, \"wifi_signal_strength\": \"\"}]','[{\"dns1\": \"172.19.3.254\", \"dns2\": \"\", \"dns3\": \"\", \"nic_name\": \"Intel(R) Ethernet Connection (2) I219-LM\", \"ipv4_addr\": \"172.19.3.137\", \"ipv6_addr\": \"fe80::99fc:6ee3:797f:71aa\", \"mac_address\": \"6C:4B:90:FC:CF:22\"}]','[{\"free\": 95389626368, \"drive\": \"C:\", \"total\": 126696288256}]','[{\"mem_free\": 6019379200, \"mem_total\": 8476868608, \"mem_v1plugin\": 74878976, \"mem_v1collector\": 0}]','[{\"zaapi\": 0, \"panacast\": -1, \"team_room\": -1, \"zoom_room\": 1, \"v1collector\": -1, \"logitech_sync\": -1}, {\"v1plugin_failover\": false, \"v1plugin_selfhealing\": false, \"v1plugin_tracefeature\": false}]','2.0.0.6','2023-05-03 03:06:58','2023-09-07 06:59:06',10,10,30,NULL,0,'{\"user\": null, \"call_status\": true}'),(484,NULL,36,'A4:17:91:24:C7:89',NULL,NULL,NULL,'Ethernet 802.3','3axg9kr',1,-1,'2023-05-25 18:25:02',1,'[{\"cpu\": 8, \"cpu_total\": 21, \"cpu_v1plugin\": 0, \"cpu_v1collector\": 0}]','[{\"mac\": \"A4:17:91:24:C7:89\", \"os_sn\": \"00436-20004-20046-AAOEM\", \"domain\": \"WORKGROUP\", \"bios_sn\": \"806046D120003946\", \"hostname\": \"ZOOMZOO-CDBT8FB\", \"timezone\": \"-04:00:00\", \"utc_time\": \"2023-06-12T17:55:07.9493715Z\", \"user_name\": \"ZOOMZOO-CDBT8FB\\\\ZoomRoomsAdmin\", \"wifi_ssid\": \"\", \"ip_address\": \"10.1.12.53\", \"win_os_ver\": \"10.0.19042\", \"last_reboot\": \"2023-06-12T17:52:16.5Z\", \"master_volume\": 100, \"wifi_signal_strength\": \"\"}]','[{\"dns1\": \"10.1.240.51\", \"dns2\": \"10.1.240.52\", \"dns3\": \"\", \"nic_name\": \"Intel(R) Ethernet Connection (6) I219-LM\", \"ipv4_addr\": \"10.1.12.53\", \"ipv6_addr\": \"fe80::5422:6bef:ada4:ee87\", \"mac_address\": \"A4:17:91:24:C7:89\"}]','[{\"free\": 103562719232, \"drive\": \"C:\", \"total\": 127719698432}, {\"free\": 6693224448, \"drive\": \"D:\", \"total\": 15709765632}]','[{\"mem_free\": 5311647744, \"mem_total\": 8377806848, \"mem_v1plugin\": 0, \"mem_v1collector\": 0}]','[{\"zaapi\": 0, \"panacast\": -1, \"team_room\": -1, \"zoom_room\": 1, \"v1collector\": -1, \"logitech_sync\": -1}, {\"v1plugin_failover\": false, \"v1plugin_selfhealing\": true, \"v1plugin_tracefeature\": false}]','2.0.0.6','2023-05-25 18:25:02','2023-06-12 17:55:08',10,10,30,NULL,0,NULL),(486,NULL,36,'A4:17:91:27:81:3D',NULL,NULL,NULL,'Ethernet 802.3','4r42nsq',1,1,'2023-08-04 18:12:02',1,'[{\"cpu\": 8, \"cpu_total\": 6, \"cpu_v1plugin\": 0, \"cpu_v1collector\": 0}]','[{\"mac\": \"A4:17:91:27:81:3D\", \"os_sn\": \"00436-20010-55066-AAOEM\", \"domain\": \"WORKGROUP\", \"bios_sn\": \"806049E080000907\", \"hostname\": \"DESKTOP-5P9GGA5\", \"timezone\": \"-04:00:00\", \"utc_time\": \"2023-08-04T18:27:03.66407Z\", \"user_name\": \"DESKTOP-5P9GGA5\\\\Admin\", \"wifi_ssid\": \"\", \"ip_address\": \"10.9.155.21\", \"win_os_ver\": \"10.0.19044\", \"last_reboot\": \"2023-08-04T18:23:29.5Z\", \"master_volume\": 0, \"wifi_signal_strength\": \"\"}]','[{\"dns1\": \"10.3.10.3\", \"dns2\": \"10.1.10.3\", \"dns3\": \"\", \"nic_name\": \"Realtek PCIe GbE Family Controller\", \"ipv4_addr\": \"10.9.155.21\", \"ipv6_addr\": \"fe80::e9a2:d010:72d2:3c92\", \"mac_address\": \"A4:17:91:27:81:3D\"}]','[{\"free\": 52059627520, \"drive\": \"C:\", \"total\": 127719698432}]','[{\"mem_free\": 4480217088, \"mem_total\": 8384733184, \"mem_v1plugin\": 0, \"mem_v1collector\": 0}]','[{\"zaapi\": -1, \"panacast\": -1, \"team_room\": -1, \"zoom_room\": -1, \"v1collector\": -1, \"logitech_sync\": -1}, {\"v1plugin_failover\": false, \"v1plugin_selfhealing\": false, \"v1plugin_tracefeature\": false}]','2.0.0.4','2023-08-04 18:12:02','2023-08-04 18:27:04',10,10,30,NULL,0,NULL),(488,NULL,36,'80:00:0B:2E:AC:A2',NULL,NULL,NULL,'Ethernet 802.3','jwku9jj',1,1,'2023-08-22 12:15:42',1,'[{\"cpu\": 4, \"cpu_total\": 99, \"cpu_v1plugin\": 0, \"cpu_v1collector\": 0}]','[{\"mac\": \"80:00:0B:2E:AC:A2\", \"os_sn\": \"00330-80000-00000-AA135\", \"domain\": \"WORKGROUP\", \"bios_sn\": \"CNU4359KFF\", \"hostname\": \"DESKTOP-OMHL5SK\", \"timezone\": \"05:00:00\", \"utc_time\": \"2023-09-14T04:57:44.7368916Z\", \"user_name\": \"DESKTOP-OMHL5SK\\\\AZAN LAPTOP STORE\", \"wifi_ssid\": \"Yasir\", \"ip_address\": \"192.168.18.15\", \"win_os_ver\": \"10.0.19045\", \"last_reboot\": \"2023-09-07T12:55:43.5Z\", \"master_volume\": 100, \"wifi_signal_strength\": \"99%\"}]','[{\"dns1\": \"\", \"dns2\": \"\", \"dns3\": \"\", \"nic_name\": \"VMware Virtual Ethernet Adapter for VMnet1\", \"ipv4_addr\": \"192.168.88.1\", \"ipv6_addr\": \"fe80::9954:918a:f8d3:5254\", \"mac_address\": \"00:50:56:C0:00:01\"}, {\"dns1\": \"\", \"dns2\": \"\", \"dns3\": \"\", \"nic_name\": \"VMware Virtual Ethernet Adapter for VMnet8\", \"ipv4_addr\": \"192.168.60.1\", \"ipv6_addr\": \"fe80::9201:e64b:ed0d:184f\", \"mac_address\": \"00:50:56:C0:00:08\"}, {\"dns1\": \"192.168.18.1\", \"dns2\": \"\", \"dns3\": \"\", \"nic_name\": \"Intel(R) Centrino(R) Advanced-N 6235\", \"ipv4_addr\": \"192.168.18.15\", \"ipv6_addr\": \"fe80::4fb1:a14d:9c06:4eb1\", \"mac_address\": \"80:00:0B:2E:AC:A2\"}]','[{\"free\": 11474100224, \"drive\": \"C:\", \"total\": 127381204992}, {\"free\": 92130320384, \"drive\": \"D:\", \"total\": 499497562112}, {\"free\": 0, \"drive\": \"E:\", \"total\": 0}]','[{\"mem_free\": 1966084096, \"mem_total\": 17036783616, \"mem_v1plugin\": 105275392, \"mem_v1collector\": 0}]','[{\"zaapi\": -1, \"panacast\": -1, \"team_room\": -1, \"zoom_room\": -1, \"v1collector\": -1, \"logitech_sync\": -1}, {\"v1plugin_failover\": false, \"v1plugin_selfhealing\": false, \"v1plugin_tracefeature\": false}]','2.0.1.0','2023-08-22 12:15:42','2023-09-14 04:57:56',10,10,30,NULL,0,NULL),(489,NULL,36,'F8:75:A4:D8:5D:93',NULL,NULL,'San Antonio - Lenovo','Ethernet 802.3','homv6xv',1,1,'2023-09-13 02:58:19',1,'[{\"cpu\": 6, \"cpu_total\": 0, \"cpu_v1plugin\": 0, \"cpu_v1collector\": 0}]','[{\"mac\": \"F8:75:A4:D8:5D:93\", \"os_sn\": \"00436-20001-43531-AAOEM\", \"domain\": \"WORKGROUP\", \"bios_sn\": \"MJ0DD014\", \"hostname\": \"DESKTOP-C0SH8DL\", \"timezone\": \"-07:00:00\", \"utc_time\": \"2023-09-14T06:23:48.6605812Z\", \"user_name\": \"DESKTOP-C0SH8DL\\\\Skype\", \"wifi_ssid\": \"\", \"ip_address\": \"172.19.3.80\", \"win_os_ver\": \"10.0.22621\", \"last_reboot\": \"2023-09-13T09:32:44.5Z\", \"master_volume\": 50, \"wifi_signal_strength\": \"\"}]','[{\"dns1\": \"172.19.3.254\", \"dns2\": \"\", \"dns3\": \"\", \"nic_name\": \"Intel(R) Ethernet Connection (7) I219-LM\", \"ipv4_addr\": \"172.19.3.80\", \"ipv6_addr\": \"fe80::e669:8218:3e17:c3b3\", \"mac_address\": \"F8:75:A4:D8:5D:93\"}]','[{\"free\": 87084449792, \"drive\": \"C:\", \"total\": 125947867136}]','[{\"mem_free\": 3119263744, \"mem_total\": 8419811328, \"mem_v1plugin\": 194494464, \"mem_v1collector\": 0}]','[{\"zaapi\": -1, \"panacast\": -1, \"team_room\": 1, \"zoom_room\": -1, \"v1collector\": -1, \"logitech_sync\": 1}, {\"v1plugin_failover\": false, \"v1plugin_selfhealing\": false, \"v1plugin_tracefeature\": false}]','2.1.5.0','2023-09-13 02:58:19','2023-09-14 06:23:49',10,10,30,NULL,0,'{\"user\": {\"userid\": \"08b022ab-daee-49e8-9ede-841867aa16a8__86400e61-d8da-4078-b97b-62b258184e5f\", \"username\": \"salenovo@visibility.one\"}, \"call_status\": true}');
/*!40000 ALTER TABLE `tbl_plugin` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:57
