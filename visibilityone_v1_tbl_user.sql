-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_user`
--

DROP TABLE IF EXISTS `tbl_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_user` (
  `user_id` int unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(127) DEFAULT NULL,
  `last_name` varchar(127) DEFAULT NULL,
  `email` varchar(127) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `username` varchar(127) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `company_id` int unsigned DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `active` int DEFAULT '1',
  `accesslevel` enum('ADMIN','MANAGER','USER') DEFAULT 'USER',
  `login` int DEFAULT '1',
  `global_admin` int DEFAULT '0',
  `registration_code` varchar(127) DEFAULT '',
  `reset_code` varchar(127) DEFAULT '',
  `reset_code_datetime` datetime DEFAULT NULL,
  `tutorial` int DEFAULT '1',
  `tutorial_download` int DEFAULT '0',
  `view_token` varchar(127) DEFAULT NULL,
  `alerts` int DEFAULT '1',
  `is_human` tinyint DEFAULT '1',
  PRIMARY KEY (`user_id`),
  KEY `company_idx` (`company_id`),
  KEY `email_idx` (`company_id`,`active`,`alerts`),
  CONSTRAINT `company_user` FOREIGN KEY (`company_id`) REFERENCES `tbl_company` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9920 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_user`
--

LOCK TABLES `tbl_user` WRITE;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` VALUES (106,'Frankie','Alvarado','frankie@visibility.one','d91dcca8410241c8d276b46079c722f3',NULL,'+13109065700',36,'2019-01-07 21:48:26',1,'ADMIN',1,1,'','g54tffk85ue7jrvs0gy2','2022-08-23 15:30:14',0,1,'c0qazttd20h3sc47cv2gq1p5arnmwdvwm29t1enr',1,1),(107,'Jose','De La Paz','jose@visibility.one','892880924fd152a3ae80d50e601186ed',NULL,'(310)467-0702',36,'2019-01-11 06:08:26',1,'ADMIN',1,1,'','5z4fei07qq6fc5d7js0e','2020-10-13 21:12:00',0,0,'ystggop7m6qvt78cnzo6jt9lt3ouving3q3zber9',0,1),(443,'John','Doe','demo@libav.com','d91dcca8410241c8d276b46079c722f3',NULL,'3109065700',36,'2020-03-05 01:19:22',1,'ADMIN',0,0,'','',NULL,0,0,'',0,1),(793,'axe1','axe1','rhmad@visibility.one','f5bb0c8de146c67b44babbf4e6584cc0',NULL,'+639557259185',36,'2020-10-31 04:58:09',1,'ADMIN',1,1,'','g40enrlcp7l4hr2tmab','2023-05-04 11:14:14',0,1,'m3db3tz7s0eov1ca5k4mfy6ry93gq0i9ifc5e9u',1,1),(797,'Mark','Salazar','msalazar@visibility.one','b9ea3714731b70db6cf91e4b1ca433b6',NULL,NULL,36,'2020-11-03 06:32:27',1,'USER',0,0,'','5czkm6ompgsd7jucuwqo','2022-07-29 02:19:54',0,1,'',0,1),(804,'Ken','Gorro','kgorro@visibility.one','f5bb0c8de146c67b44babbf4e6584cc0',NULL,NULL,36,'2021-02-09 12:55:16',1,'ADMIN',1,0,'','d6givkwjog0bcg0cch1j','2023-03-29 06:47:29',0,1,'',0,1),(833,'EmailTest1Contact','EmailTest1Contact','EmailTest1@visibility.one','f5bb0c8de146c67b44babbf4e6584cc0',NULL,'123123123',386,'2021-07-20 12:42:36',1,'ADMIN',1,0,'','',NULL,0,0,'',1,1),(924,'test123123','test123123','EmailTest3@visibility.one','f5bb0c8de146c67b44babbf4e6584cc0',NULL,'1111',441,'2022-02-11 13:04:56',1,'ADMIN',1,0,'q3y816gfi09wg9pfr8ly','',NULL,1,0,'',1,1),(925,'test11','test11','EmailTest4@visibility.one','f5bb0c8de146c67b44babbf4e6584cc0',NULL,'11111',442,'2022-02-11 13:05:29',1,'ADMIN',1,0,'rley817m94k3j5s9ku8','',NULL,1,0,'3nnzn2564voi76yypb8xm0xdrc5ewf3x00rhujr',1,1),(926,'Jose','Delapaz','jdelapazus@demo.com',NULL,NULL,'3104670702',443,'2022-02-11 15:25:43',1,'ADMIN',0,0,'','',NULL,1,0,'j3kfzbdow6xsvrvt51gvojdx85omw8985s40enq',1,1),(934,'Cory','Briggs','cory.briggs@wescodist.com','f1bde8c3794db72268fe55ecdf0b429b',NULL,NULL,445,'2022-06-03 01:49:05',1,'ADMIN',1,1,'','',NULL,0,1,'',0,1),(935,'Travis','Simpson','tsimpson@wescodist.com','a581f151865538374423bc4dd79b3b5f',NULL,NULL,445,'2022-06-03 01:49:05',1,'ADMIN',1,1,'','',NULL,0,1,'',0,1),(937,'Geoffrey','Favario','gfavario@videlio.com','196fff1b5c0b82b69488e1eeee46b088',NULL,'0786963088',446,'2022-06-13 12:33:05',1,'ADMIN',1,0,'','',NULL,1,0,NULL,1,1),(938,'Jonathan','Toribio','jonathan@visibility.one','cdda60b50856d1266b1731c5daec1498',NULL,'9414163662',36,'2022-06-15 05:47:41',1,'ADMIN',1,1,'','',NULL,0,1,NULL,1,1),(940,'John','Doe','john@acme.com',NULL,NULL,'111111111',447,'2022-07-18 17:17:00',1,'ADMIN',0,0,'','',NULL,1,0,'',1,1),(944,'AdminPerson','AdminPerson1','AdminPerson1@demo.com',NULL,NULL,'+38067746516516',449,'2022-07-20 08:42:47',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(946,'Yasir','Amin','yamin@visibility.one','d91123da15827c5ff2c97d6d1a1c4ed2',NULL,'+923234743622',36,'2022-08-02 03:27:28',1,'ADMIN',1,0,'','',NULL,0,1,'',1,1),(947,'Joe','Doe','teamup@coke.com',NULL,NULL,'13104670702',450,'2022-08-04 16:36:35',1,'ADMIN',0,0,'','',NULL,1,0,'',1,1),(973,'test1','test1','EmailTest7@visibility.one','f5bb0c8de146c67b44babbf4e6584cc0',NULL,NULL,36,'2020-10-31 04:58:09',1,'ADMIN',1,1,'','6ipq8qmlwcsmhn4eb23','2022-09-13 00:14:23',0,1,'',0,1),(974,'test2','test2','EmailTest8@visibility.one','f5bb0c8de146c67b44babbf4e6584cc0',NULL,NULL,36,'2023-01-19 03:24:23',1,'ADMIN',1,1,'','',NULL,0,1,NULL,1,1),(977,'Malik','Ahsan','ahsan@visibility.one',NULL,NULL,'+923450345644',36,'2023-04-05 11:50:08',0,'USER',0,0,'','',NULL,1,0,NULL,1,1),(980,'Test','Test end','temp@testi.com',NULL,NULL,'090078601',479,'2023-04-27 01:34:26',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(981,'test','test end','temp1@test2.com',NULL,NULL,'090078601',480,'2023-04-27 01:37:44',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(982,'test','test end','temp@test4.com',NULL,NULL,'090078601',481,'2023-04-27 01:39:03',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(983,'Tester','Tesster end','abc@test6.com',NULL,NULL,'090078601',482,'2023-04-27 01:42:10',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(984,'Tester first','Tester last','Tester@test9.com',NULL,NULL,'090078601',483,'2023-04-27 01:49:14',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(985,'ttester first','tester last','test@test12.com',NULL,NULL,'090078601',484,'2023-04-27 01:53:10',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(987,'Tester first','tester','t@tester13.com',NULL,NULL,'090078601',486,'2023-04-27 01:55:50',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(989,'logger','last','log@logger.com',NULL,NULL,'0900',488,'2023-04-27 02:44:06',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(990,'Logger','Lost','test@logger2.com','f5bb0c8de146c67b44babbf4e6584cc0',NULL,'0900',489,'2023-04-27 02:45:52',1,'ADMIN',0,0,'','',NULL,1,0,'',1,1),(991,'Pariatur Minima sit','Ratione ea beatae iu','e@temp.com',NULL,NULL,'Perferendis voluptat',490,'2023-04-27 02:50:25',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(992,'Officia cillum Nam m','Animi id excepturi ','temp@ok.com',NULL,NULL,'Atque ut possimus v',491,'2023-04-27 02:52:42',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(993,'Voluptatem Ipsa vo','Eos esse illum mi','a@temporary.com',NULL,NULL,'Voluptate omnis et q',492,'2023-04-27 03:05:48',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(995,'Eu sunt occaecat en','Sed dolorem sunt eum','a@temp78.com',NULL,NULL,'at@temp78.com',494,'2023-04-27 03:18:22',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(996,'Eu sunt occaecat en','Sed dolorem sunt eum','a@temp79.com',NULL,NULL,'at@temp79.com',495,'2023-04-27 03:21:02',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(997,'Do magni qui expedit','Voluptatibus sit con','b@j.com',NULL,NULL,'Esse doloremque aut ',496,'2023-04-27 03:30:42',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(1000,'Adipisicing voluptas','Recusandae Id ut a','temp@test.com',NULL,NULL,'090078601',499,'2023-05-04 10:47:08',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(1001,'Adipisicing voluptas','Recusandae Id ut a','temp@test22.com',NULL,NULL,'090078601',500,'2023-05-04 10:57:26',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(1002,'Adipisicing voluptas','Recusandae Id ut a','temp@test23.com',NULL,NULL,'090078601',501,'2023-05-04 11:03:11',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(1004,'Esse non ut perspici','Officia nesciunt id','test@abc.com',NULL,NULL,'Aut nulla qui quae q',503,'2023-05-06 02:15:47',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(1005,'In vitae est cum ear','Aute laborum quis qu','mahsan@visibility.one',NULL,NULL,'Quia aut elit dolor',504,'2023-05-06 04:39:04',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(1006,'Iure est qui dolor a','Ex illum enim venia','abc@temp3.com',NULL,NULL,'Ducimus autem et do',505,'2023-05-09 09:04:24',1,'ADMIN',0,0,'','',NULL,1,0,NULL,1,1),(1007,'aqib','ali','ali@13.com','e9f472a2706ee39ba05f032072c91693',NULL,'03005546634',506,'2023-05-19 15:56:14',0,'ADMIN',1,0,'33um98023x15d2zerd8','',NULL,0,0,NULL,1,1),(1009,'anil','sidhu','anil@11133.com','e3e0e0b164ed59c430312854451d1d22',NULL,'03002211365',508,'2023-05-19 20:22:47',0,'ADMIN',1,0,'tomrq0mqi5uhl9997z1w','',NULL,0,0,'',1,1),(1010,'System','','system@gmail.com','','system','',36,'2023-07-24 23:50:50',1,'ADMIN',1,0,'','','2023-07-24 23:50:50',0,0,'',1,1),(1011,'Fahad','Mukhtar','fmukhtar@visibility.one','5d487a280d297a082a17970acff1bc15',NULL,'+923334719570',36,'2023-08-01 03:51:44',1,'ADMIN',1,1,'','',NULL,0,0,'l29t1guit9lvbmxf3x3d8sxzuyxkqow704bw86wq',1,1),(1012,'Adil','Qureshi','aqureshi@visibility.one','aece33c83f09bfd2fede11a680f00ca8',NULL,NULL,36,'2023-08-01 03:52:47',1,'ADMIN',1,1,'','',NULL,0,0,'',1,1),(1013,'Sikandar','Ali','sali@visibility.one','e5b62c06caa561a8de010699fdae1f35',NULL,NULL,36,'2023-08-01 03:52:06',1,'ADMIN',1,1,'','',NULL,0,0,NULL,1,1),(1014,'Bilal','Khan','bkhan@visibility.one','11f0530969161d16c13ce1aec1eedf1e',NULL,NULL,36,'2023-08-01 03:52:26',1,'ADMIN',1,1,'','',NULL,0,0,NULL,1,1),(9907,'Dolor duis necessita','Beatae exercitation ','a@123.com',NULL,NULL,'Nulla ut error qui d',520,'2023-08-18 14:23:07',1,'ADMIN',0,0,'c0393204-3dd2-11ee-adfc-06c0976c8b8b','',NULL,1,0,NULL,1,1),(9908,'Peter de Montfort','John James \"Jimmy\" O\'Grady','email@domain.com',NULL,NULL,'03006995500',521,'2023-08-24 11:01:27',1,'ADMIN',0,0,'9286228b-426d-11ee-9cfa-06c0976c8b8b','',NULL,1,0,NULL,1,1),(9909,'John O\'Grady','Peter de Montfort','email1@domain.com',NULL,NULL,'030006982321',522,'2023-08-25 04:22:16',1,'ADMIN',0,0,'f956da2e-42fe-11ee-9cfa-06c0976c8b8b','',NULL,1,0,NULL,1,1),(9910,'System','','system@gmail.com','','system','',36,'2023-08-29 05:36:14',1,'ADMIN',1,0,'','','2023-08-29 05:36:14',1,0,'',1,0),(9911,'Ali','Mirza','amirza@visibility.one','123456789',NULL,NULL,36,'2023-08-31 05:10:08',1,'USER',1,0,'6z1zh8f43r7gfhvm6qwx','',NULL,1,0,NULL,1,1),(9916,'Ken','Gorro','kgorrsso@visibility.one',NULL,NULL,'1111',36,'2023-09-07 20:06:11',1,'USER',0,0,'','',NULL,1,0,NULL,1,1),(9917,'Ken','Gorro','kgsdsdorro@visibility.one',NULL,NULL,'1111',36,'2023-09-07 23:22:19',1,'USER',0,0,'','',NULL,1,0,NULL,1,1);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:10
