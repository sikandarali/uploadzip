-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_direct_access_actions`
--

DROP TABLE IF EXISTS `tbl_direct_access_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_direct_access_actions` (
  `action_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `company_id` bigint unsigned DEFAULT NULL,
  `device_id` bigint unsigned DEFAULT NULL,
  `pair_code` int DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `requestor_email` varchar(100) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `active` tinyint DEFAULT '1',
  PRIMARY KEY (`action_id`),
  KEY `pair_code` (`company_id`,`device_id`,`pair_code`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_direct_access_actions`
--

LOCK TABLES `tbl_direct_access_actions` WRITE;
/*!40000 ALTER TABLE `tbl_direct_access_actions` DISABLE KEYS */;
INSERT INTO `tbl_direct_access_actions` VALUES (2,36,348,4706,'video','wakeDevice','mackiegarcia@gmail.com','2020-04-01 05:37:02',0),(3,36,348,2668,'video','rebootDevice','kss@gmail.com','2020-04-01 05:49:52',0),(4,36,348,2549,'video','rebootDevice','mackiegarcia@gmail.com','2020-04-01 05:53:09',0),(5,1234567,69,6369,'video','shagit','mackiegarcia@gmail.com','2020-04-01 05:54:35',1),(6,1234567,69,7000,'video','shagit','mackiegarcia@gmail.com','2020-04-01 05:55:40',1),(7,36,348,7580,'video','rebootDevice','mackiegarcia@gmail.com','2020-04-01 06:01:29',0),(8,36,348,4512,'video','wakeDevice','je@gmail.com','2020-04-01 06:21:01',0),(9,36,348,8478,'video','wakeDevice','makiis@gmail.com','2020-04-01 06:31:50',0),(10,36,348,3705,'video','wakeDevice','mackiegarcia@gmail.com','2020-04-01 12:41:31',0),(11,36,356,6383,'video','rebootDevice','mackie@gm.com','2020-04-01 12:49:01',0),(12,36,348,8035,'video','wakeDevice','mackiegracia@gmail.com','2020-04-05 14:41:47',0),(13,36,348,9943,'video','wakeDevice','m@gmail.com','2020-04-05 14:46:07',0),(14,36,348,5015,'video','wakeDevice','saas@gmail','2020-04-05 14:49:59',1),(15,36,348,8799,'video','rebootDevice','mackiegarcia@gmail.com','2020-04-05 15:04:25',1),(16,36,28,8917,'zoom','rebootPc','mgarcia@gma.com','2020-04-10 10:54:24',0),(17,36,28,6424,'zoom','restartZoom','r@d.com','2020-04-10 11:07:04',0),(18,36,357,5673,'video','rebootDevice','m@gmail.com','2020-04-15 02:53:50',0),(19,36,319,3728,'audio','rebootDevice','m@gmail.com','2020-04-15 05:51:32',0),(20,36,319,3111,'audio','rebootDevice','m@gmail.com','2020-04-15 06:19:00',0),(21,36,319,4162,'audio','rebootDevice','m@gmail.com','2020-04-15 06:31:30',0),(22,36,319,3396,'audio','rebootDevice','m@fmail.com','2020-04-15 06:35:36',1),(23,36,357,1353,'video','rebootDevice','jose@visibility.one','2020-04-15 17:00:09',1),(24,36,319,3679,'audio','rebootDevice','Frankie@visibility.one','2020-05-28 23:02:19',1),(25,36,360,7886,'video','rebootDevice','frankie@visibility.one','2020-07-07 18:46:57',0);
/*!40000 ALTER TABLE `tbl_direct_access_actions` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:02
