-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_collector`
--

DROP TABLE IF EXISTS `tbl_collector`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_collector` (
  `collector_id` int unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int unsigned DEFAULT NULL,
  `company_id` int unsigned DEFAULT NULL,
  `collector_name` varchar(45) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `mac_address` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `host_name` varchar(127) DEFAULT NULL,
  `network_type` varchar(45) DEFAULT NULL,
  `verified` int DEFAULT '0',
  `paused` int DEFAULT '0',
  `pair_code` int DEFAULT NULL,
  `alerts` int DEFAULT '1',
  `background_qos` int DEFAULT '1',
  `active` int DEFAULT '0',
  `status` int DEFAULT '-1',
  `create_date` datetime DEFAULT NULL,
  `verify_date` datetime DEFAULT NULL,
  `last_connect` datetime DEFAULT NULL,
  `last_message` varchar(255) DEFAULT NULL,
  `stats_cpu_usage` float DEFAULT NULL,
  `stats_mem_free` bigint DEFAULT NULL,
  `stats_mem_total` bigint DEFAULT NULL,
  `stats_hd_details` text,
  `max_threads` int DEFAULT '20',
  `request_interval` int DEFAULT '30',
  `ping_interval` int DEFAULT '10',
  `background_qos_interval` int DEFAULT '300',
  `version` varchar(45) DEFAULT NULL,
  `logging` int DEFAULT '0',
  `collector_update_schedule` datetime DEFAULT NULL,
  `scheduled_update_setting` tinyint DEFAULT '0',
  `local_utc` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`collector_id`),
  KEY `site_collector_idx` (`site_id`),
  KEY `collector_company_idx` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=681 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_collector`
--

LOCK TABLES `tbl_collector` WRITE;
/*!40000 ALTER TABLE `tbl_collector` DISABLE KEYS */;
INSERT INTO `tbl_collector` VALUES (647,671,386,NULL,NULL,'14:9D:99:83:99:42','172.19.3.143','DESKTOP-1PH4SH1','Ethernet',1,0,229639,1,1,1,-1,'2023-01-17 10:46:53','2023-01-17 10:49:52','2023-07-14 06:35:03',NULL,NULL,7537254400,19589902336,'[{\"free\": 390489878528, \"drive\": \"c:/\", \"total\": 483927257088}, {\"free\": 390489878528, \"drive\": \"d:/\", \"total\": 483927257088}]',20,30,10,300,'1.0.0.0',0,NULL,0,NULL),(648,672,386,NULL,NULL,'14:58:D0:B9:25:A5','192.168.1.14','DESKTOP-OMHL5SK','Ethernet',1,0,669651,1,1,0,-1,'2023-02-20 09:19:43','2023-02-20 09:22:09','2023-03-10 08:46:50',NULL,15,6055931904,22942363648,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":7965921280,\\\"total\\\":127381204992},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":234274705408,\\\"total\\\":499497562112}]\"',20,30,10,300,'2.1.7',0,NULL,0,NULL),(653,677,441,NULL,NULL,'14:9D:99:83:99:42','172.19.3.129','Frankies-Mini.socal.rr.com','Unknown',1,0,229639,1,1,1,-1,'2023-01-17 10:46:53','2023-01-17 10:49:52','2023-03-10 08:46:44',NULL,7.1,37241856,17179869184,'\"50\"',20,30,10,300,'2.0.9',0,NULL,0,NULL),(654,678,442,NULL,NULL,'14:9D:99:83:99:42','172.19.3.199','Frankies-Mini.socal.rr.com','Unknown',1,0,229639,1,1,1,-1,'2023-01-17 10:46:53','2023-01-17 10:49:52','2023-03-10 08:46:44',NULL,7.1,37241856,17179869184,'\"50\"',20,30,10,300,'2.0.9',0,NULL,0,NULL),(656,707,36,NULL,NULL,'14:58:D0:B9:25:A5','192.168.1.12','DESKTOP-OMHL5SK','Unknown',0,0,186821,1,1,0,1,'2023-03-16 11:07:52','2023-07-14 04:17:48','2023-07-14 04:21:04',NULL,100,7022157824,28311072768,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":5775228928,\\\"total\\\":127381204992},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":186818756608,\\\"total\\\":499497562112}]\"',20,30,10,300,'2.1.11',0,NULL,0,'UTC 05:00'),(657,NULL,NULL,NULL,NULL,'60:18:95:76:10:60','192.168.50.160','V1-Jonathan','Ethernet',0,0,464851,1,1,0,1,'2023-03-22 18:12:43',NULL,'2023-04-05 18:08:33',NULL,2,18582691840,22844698624,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":329888505856,\\\"total\\\":490766069760}]\"',20,30,10,300,'2.1.7',0,NULL,0,NULL),(662,693,36,NULL,NULL,'8C:04:BA:37:3E:3D','192.168.100.78','DESKTOP-8TSEG4F','Unknown',0,0,712256,1,1,0,1,'2023-04-05 11:47:12','2023-04-05 12:18:57','2023-09-12 22:29:41',NULL,5,33678036992,34275373056,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":82485248,\\\"total\\\":160338800640},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":24498376704,\\\"total\\\":219082125312},{\\\"drive\\\":\\\"e:/\\\",\\\"free\\\":7426293760,\\\"total\\\":103412658176}]\"',20,30,10,300,'2.1.7',0,NULL,0,' 05:00'),(663,693,36,NULL,NULL,'60:18:95:76:10:60','192.168.1.80','V1-Jonathan','Ethernet',0,0,742213,1,1,0,-1,'2023-04-05 17:38:52','2023-04-09 15:06:24','2023-04-05 17:40:22',NULL,2,20040499200,22844698624,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":330608967680,\\\"total\\\":490766069760}]\"',20,30,10,300,'2.1.7',0,NULL,0,NULL),(664,NULL,NULL,NULL,NULL,'60:18:95:76:10:60','192.168.1.80','V1-Jonathan','Ethernet',0,0,189353,1,1,0,1,'2023-04-05 17:40:34',NULL,'2023-05-15 21:03:08',NULL,3,11185045504,29368782848,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":323173912576,\\\"total\\\":490766069760}]\"',20,30,10,300,'2.1.7',0,NULL,0,'UTC-04:00'),(665,699,36,NULL,NULL,'00:1F:C6:9D:01:F5','172.19.3.116','DESKTOP-3KFSIAC','Ethernet',0,0,702006,1,1,0,-1,'2023-04-06 09:45:11','2023-04-17 14:09:14','2023-04-17 14:24:20',NULL,2,7491002368,19613679616,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":146561536000,\\\"total\\\":248499388416}]\"',20,30,10,300,'2.1.8',0,NULL,0,'UTC-07:00'),(668,700,36,NULL,NULL,'00:1F:C6:9D:01:F5','172.19.3.116','DESKTOP-3KFSIAC','Ethernet',0,0,959232,1,1,0,1,'2023-04-17 14:40:18','2023-04-17 14:42:21','2023-07-11 18:36:54',NULL,9,9857568768,19613679616,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":151465652224,\\\"total\\\":248499388416}]\"',20,30,10,300,'2.1.11',0,NULL,0,'UTC-07:00'),(669,705,36,NULL,NULL,'94:C6:91:0F:99:D3','172.19.3.113','visibilityONE-PC','Ethernet',0,0,397939,1,1,0,1,'2023-04-21 05:32:42','2023-06-02 17:59:42','2023-07-14 07:37:08',NULL,8,3383160832,9834868736,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":864728227840,\\\"total\\\":999609593856},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":-1,\\\"total\\\":-1}]\"',20,30,10,300,'2.1.11',0,NULL,0,'UTC-07:00'),(670,NULL,NULL,NULL,NULL,'12:34:56:123','123.123.121',NULL,NULL,0,0,960048,1,1,0,-1,'2023-05-30 11:39:44',NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,30,10,300,NULL,0,NULL,0,NULL),(671,NULL,NULL,NULL,NULL,'14:58:D0:B9:25:A1','192.168.1.125',NULL,NULL,0,0,157600,1,1,0,-1,'2023-05-30 14:01:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,20,30,10,300,NULL,0,NULL,0,NULL),(672,735,36,NULL,NULL,'14:58:D0:B9:25:A5','192.168.18.15','DESKTOP-OMHL5SK','Ethernet',1,0,275613,1,1,1,1,'2023-07-14 04:28:42','2023-09-08 14:10:47','2023-09-08 16:58:04',NULL,48,20848328704,29921685504,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":11674861568,\\\"total\\\":127381204992},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":95095496704,\\\"total\\\":499497562112}]\"',20,30,10,300,'2.1.13',0,NULL,0,' 05:00'),(673,NULL,NULL,NULL,NULL,'00:1F:C6:9D:01:F5','172.19.3.119','DESKTOP-3KFSIAC','Ethernet',0,0,924191,1,1,0,1,'2023-07-14 05:00:25',NULL,'2023-07-14 05:02:42',NULL,0,4128145408,19613679616,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":152669249536,\\\"total\\\":248499388416}]\"',20,30,10,300,'2.1.11',0,NULL,0,'UTC-07:00'),(674,720,36,NULL,NULL,'00:1F:C6:9D:01:F5','172.19.3.119','DESKTOP-3KFSIAC','Ethernet',0,0,426259,1,1,0,1,'2023-07-14 05:02:47','2023-08-04 14:29:54','2023-08-10 17:45:36',NULL,0,3899174912,19613679616,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":148553711616,\\\"total\\\":248499388416}]\"',20,30,10,300,'2.1.11',0,NULL,0,'UTC-07:00'),(675,717,36,NULL,NULL,'94:C6:91:0F:99:D3','172.19.3.113','visibilityONE-PC','Ethernet',0,0,672213,1,1,0,1,'2023-07-22 05:30:34','2023-07-22 05:32:02','2023-08-05 04:07:46',NULL,4,3085557760,9834868736,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":864320987136,\\\"total\\\":999609593856},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":-1,\\\"total\\\":-1}]\"',20,30,10,300,'2.1.11',0,NULL,0,'-07:00'),(676,732,36,NULL,NULL,'94:C6:91:0F:99:D3','172.19.3.113','visibilityONE-PC','Ethernet',1,0,759248,1,1,1,1,'2023-08-05 04:07:51','2023-09-03 03:31:33','2023-09-14 06:23:56',NULL,1,3309776896,9834868736,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":859834490880,\\\"total\\\":999609593856},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":-1,\\\"total\\\":-1}]\"',20,30,10,300,'2.1.12',0,NULL,0,'-07:00'),(677,722,36,NULL,NULL,'00:FF:88:84:60:33','172.19.3.108','DESKTOP-BD02M9C','Ethernet',1,0,549256,1,1,1,1,'2023-08-08 06:04:33','2023-08-08 06:06:17','2023-09-14 06:24:01',NULL,2,2813849600,28893773824,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":163823185920,\\\"total\\\":213738500096},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":776392802304,\\\"total\\\":785382371328},{\\\"drive\\\":\\\"e:/\\\",\\\"free\\\":-1,\\\"total\\\":-1}]\"',20,30,10,300,'2.1.11',0,NULL,0,'-07:00'),(678,723,36,NULL,NULL,'06:5B:09:98:54:67','10.0.1.169','EC2AMAZ-HS45TG7','Unknown',1,0,122188,1,1,1,1,'2023-08-08 17:02:51','2023-08-08 17:05:16','2023-08-09 07:10:10',NULL,0,1438642176,5770952704,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":41687646208,\\\"total\\\":64422408192}]\"',20,30,10,300,'2.1.11',0,NULL,0,' 00:00'),(679,725,36,NULL,NULL,'00:0D:3A:06:82:73','10.3.0.4','TestWindows2019','Ethernet',1,0,526525,1,1,1,1,'2023-08-10 13:21:22','2023-08-10 13:22:42','2023-09-14 06:24:03',NULL,1,1421217792,9878466560,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":118217932800,\\\"total\\\":135771664384},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":15770681344,\\\"total\\\":17177767936},{\\\"drive\\\":\\\"e:/\\\",\\\"free\\\":-1,\\\"total\\\":-1}]\"',20,30,10,300,'2.1.12',0,NULL,0,' 00:00'),(680,734,36,NULL,NULL,'E4:B9:7A:31:16:C4','192.168.100.135','DESKTOP-NCVC7KD','Ethernet',1,0,166617,1,1,1,1,'2023-09-08 11:17:44','2023-09-08 11:21:25','2023-09-14 06:23:57',NULL,65,11557322752,19584208896,'\"[{\\\"drive\\\":\\\"c:/\\\",\\\"free\\\":349365231616,\\\"total\\\":511352762368},{\\\"drive\\\":\\\"d:/\\\",\\\"free\\\":162032537600,\\\"total\\\":262143995904},{\\\"drive\\\":\\\"g:/\\\",\\\"free\\\":237855113216,\\\"total\\\":237960687616}]\"',20,30,10,300,'2.1.11',0,NULL,0,' 05:00');
/*!40000 ALTER TABLE `tbl_collector` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:24:05
