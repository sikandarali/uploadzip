-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_plugin_version`
--

DROP TABLE IF EXISTS `tbl_plugin_version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_plugin_version` (
  `plugin_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`plugin_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_plugin_version`
--

LOCK TABLES `tbl_plugin_version` WRITE;
/*!40000 ALTER TABLE `tbl_plugin_version` DISABLE KEYS */;
INSERT INTO `tbl_plugin_version` VALUES (1,'0.11.2.64','VisibilityOnePluginSetup-0.11.2.64.exe','2020-06-06 06:06:40',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(2,'1.0.8.5','VisibilityOnePluginSetup-1.0.8.5.exe','2020-09-15 00:09:40',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(3,'1.0.8.7','VisibilityOnePluginSetup-1.0.8.7.exe','2020-09-19 04:12:40',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(4,'1.0.8.8','VisibilityOnePluginSetup-1.0.8.8.exe','2020-09-21 09:52:40',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(5,'1.0.12.0','VisibilityOnePluginSetup-1.0.12.exe','2020-10-04 09:52:40',0,NULL,NULL,NULL,NULL,'COMPLETED',0),(8,'1.0.31.0','VisibilityOnePluginSetup-1.0.31.exe','2020-10-24 00:43:37',0,'19MB','test release notes','test email notes','2020-10-24 05:43:00','COMPLETED',0),(9,'1.0.32.0','VisibilityOnePluginSetup-1.0.32.0.exe','2020-10-24 06:47:30',0,'19MB',NULL,NULL,NULL,'COMPLETED',0),(10,'1.0.33.0','VisibilityOnePluginSetup-1.0.33.0.exe','2020-10-24 06:47:41',0,'19MB',NULL,'ssas','2020-10-30 06:20:00','COMPLETED',0),(13,'1.0.34.0','VisibilityOnePluginSetup-1.0.34.0.exe','2020-11-03 22:20:06',0,'19MB','notes',NULL,'2020-11-07 06:35:00','COMPLETED',0),(14,'1.0.35.0','VisibilityOnePluginSetup-1.0.35.0.exe','2020-11-24 04:03:06',0,'19MB','Added Zip Data (Zip VisibilityOne Plugin Files) button',NULL,NULL,'COMPLETED',0),(15,'1.0.36.0','VisibilityOnePluginSetup-1.0.36.0.exe','2021-03-06 01:43:28',0,'19MB','Added fix for unable to send data',NULL,'2021-03-19 04:00:00','COMPLETED',0),(16,'1.0.37.0','VisibilityOnePluginSetup-1.0.37.0.exe','2021-03-24 23:29:34',0,'19MB','Added fix for cpu info',NULL,'2021-03-30 07:53:00','COMPLETED',0),(17,'1.0.38.0','VisibilityOnePluginSetup-1.0.38.0.exe','2021-03-30 09:02:46',0,'19MB','Fixed for cpu info error when collector is not installed',NULL,'2021-03-30 09:07:00','COMPLETED',0),(18,'1.0.39.0','VisibilityOnePluginSetup-1.0.39.0.exe','2021-04-24 07:02:30',0,'19MB','Added silent installation',NULL,NULL,'COMPLETED',0),(19,'1.0.40.0','VisibilityOnePluginSetup-1.0.40.0.exe','2021-04-29 07:57:53',0,'19MB','Fixed ping error caused by unavailable audio render',NULL,NULL,'COMPLETED',0),(20,'1.0.41.0','VisibilityOnePluginSetup-1.0.41.0.exe','2021-05-03 22:37:25',0,'19MB','CPU usage optimization',NULL,NULL,'COMPLETED',0),(21,'1.0.42.0','VisibilityOnePluginSetup-1.0.42.0.exe','2021-05-27 12:16:50',0,'19MB','Added unlink plugin',NULL,'2021-05-30 02:17:00','COMPLETED',0),(22,'1.0.43.0','VisibilityOnePluginSetup-1.0.43.0.exe','2021-07-05 22:01:19',0,'19MB','Added wifi, mac, ip host info',NULL,'2021-08-08 00:23:00','COMPLETED',0),(23,'1.0.44.0','VisibilityOnePluginSetup-1.0.44.0.exe','2021-11-14 23:06:59',0,'22MB','Added teams rooms support',NULL,NULL,'COMPLETED',0),(24,'1.0.45.0','VisibilityOnePluginSetup-1.0.45.0.exe','2021-11-15 01:38:07',0,'22MB','Disabled Collector api Server',NULL,NULL,'COMPLETED',0),(25,'1.0.47.0','VisibilityOnePluginSetup-1.0.47.0.exe','2022-03-09 21:31:10',0,'22MB','Removed pathsolution calls in app simulator',NULL,NULL,'COMPLETED',0),(26,'1.0.48.0','VisibilityOnePluginSetup-1.0.48.0.exe','2022-03-10 07:49:24',0,'22MB','Added call simulator actions',NULL,'2022-03-19 16:00:00','COMPLETED',0),(27,'1.0.49.0','VisibilityOnePluginSetup-1.0.49.0.exe','2022-03-21 08:13:38',0,'22MB','Added crestron client list',NULL,NULL,'COMPLETED',0),(28,'1.0.50.0','VisibilityOnePluginSetup-1.0.50.0.exe','2022-03-31 10:54:11',0,'22MB','Fixed login error in some PCs due to mac address',NULL,NULL,'COMPLETED',0),(29,'1.0.51.0','VisibilityOnePluginSetup-1.0.51.0.exe','2022-04-07 12:08:53',0,'22MB','Fixed disconnection issue due to cpu info error in WMI',NULL,NULL,'COMPLETED',0),(30,'1.0.52.0','VisibilityOnePluginSetup-1.0.52.0.exe','2022-04-25 17:25:42',0,'22MB','Changed determining of teams room installation',NULL,NULL,'COMPLETED',0),(31,'1.0.58.0','VisibilityOnePluginSetup-1.0.58.0.exe','2023-02-13 00:00:00',0,'23.8MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(32,'1.0.60.0','VisibilityOnePluginSetup-1.0.60.0.exe','2023-02-21 21:00:00',0,'29.8MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(33,'2.0.0.3','VisibilityOnePluginSetup-2.0.0.3.exe','2023-04-21 21:00:00',0,'29.8MB','Minor Bug Fixes',NULL,NULL,'COMPLETED',0),(34,'2.0.0.4','VisibilityOnePluginSetup-2.0.0.4.exe','2023-05-21 21:00:00',0,'29.8MB','Minor Bug Fixes',NULL,'2023-05-24 07:24:00','SCHEDULED',0),(35,'2.0.0.6','VisibilityOnePluginSetup-2.0.0.6.exe','2023-05-22 21:00:00',1,'29.8MB','Minor Bug Fixes',NULL,'2023-05-25 07:31:00','UPDATING',0);
/*!40000 ALTER TABLE `tbl_plugin_version` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:11
