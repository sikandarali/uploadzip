-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_desktop_plugin_versions`
--

DROP TABLE IF EXISTS `tbl_desktop_plugin_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_desktop_plugin_versions` (
  `desktop_plugin_version_id` int NOT NULL AUTO_INCREMENT,
  `version` varchar(127) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `stamp` datetime DEFAULT CURRENT_TIMESTAMP,
  `active` int DEFAULT '0',
  `file_size` varchar(50) DEFAULT NULL,
  `release_notes` longtext,
  `email_notes` longtext,
  `update_schedule` datetime DEFAULT NULL,
  `update_status` enum('OPEN','SCHEDULED','UPDATING','COMPLETED','FAILED') DEFAULT NULL,
  `include_release_notes` tinyint DEFAULT '0',
  PRIMARY KEY (`desktop_plugin_version_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_desktop_plugin_versions`
--

LOCK TABLES `tbl_desktop_plugin_versions` WRITE;
/*!40000 ALTER TABLE `tbl_desktop_plugin_versions` DISABLE KEYS */;
INSERT INTO `tbl_desktop_plugin_versions` VALUES (1,'1.0.0.0','VisibilityOneDesktopPluginSetup-1.0.0.0.exe','2020-12-07 23:53:07',0,'19MB','Inital release',NULL,NULL,'COMPLETED',0),(2,'1.0.2.0','VisibilityOneDesktopPluginSetup-1.0.2.0.exe',NULL,0,'20MB','New actions',NULL,NULL,'COMPLETED',0),(3,'1.0.3.0','VisibilityOneDesktopPluginSetup-1.0.3.0.exe','2020-12-10 13:22:57',0,'20MB','App launch fixes',NULL,NULL,'COMPLETED',0),(4,'1.0.4.0','VisibilityOneDesktopPluginSetup-1.0.4.0.exe',NULL,0,'20MB','Remote heal actions and icon changes',NULL,NULL,'COMPLETED',0),(5,'1.0.5.0','VisibilityOneDesktopPluginSetup-1.0.5.0.exe','2020-12-11 04:52:54',0,'20MB','Update desktop plugin version checking',NULL,NULL,'COMPLETED',0),(6,'1.0.6.0','VisibilityOneDesktopPluginSetup-1.0.6.0.exe','2020-12-16 07:22:23',0,'21MB','Fixed error for unmatched action params',NULL,NULL,'COMPLETED',0),(7,'1.0.7.0','VisibilityOneDesktopPluginSetup-1.0.7.0.exe','2020-12-16 23:38:40',0,'21MB','Added datetime in Remote heal result',NULL,NULL,'COMPLETED',0),(8,'1.0.8.0','VisibilityOneDesktopPluginSetup-1.0.8.0.exe',NULL,0,'21MB','Bugfix and Added status and message in Call simulator results',NULL,NULL,'COMPLETED',0),(9,'1.0.9.0','VisibilityOneDesktopPluginSetup-1.0.9.0.exe','2020-12-18 01:27:40',0,'21MB','Changed SimulateCallQoS link-troubleshoot to 30s',NULL,NULL,'COMPLETED',0),(10,'1.0.10.0','VisibilityOneDesktopPluginSetup-1.0.10.0.exe','2020-12-23 14:25:36',0,'21MB','New zoom data CallSimulator json changes',NULL,NULL,'COMPLETED',0),(11,'1.0.11.0','VisibilityOneDesktopPluginSetup-1.0.11.0.exe','2020-12-23 15:00:26',0,'21MB','New zoom data CallSimulator changes',NULL,NULL,'COMPLETED',0),(12,'1.0.12.0','VisibilityOneDesktopPluginSetup-1.0.12.0.exe','2020-12-23 16:06:34',0,'21MB','Remote heal fix',NULL,NULL,'COMPLETED',0),(13,'1.0.13.0','VisibilityOneDesktopPluginSetup-1.0.13.0.exe','2021-01-11 23:48:53',0,'21MB','Call sim and ping interval fixes',NULL,NULL,'COMPLETED',0),(14,'1.0.14.0','VisibilityOneDesktopPluginSetup-1.0.14.0.exe','2021-01-30 04:01:40',0,'21MB','Call simulator changes 2',NULL,'2021-02-02 05:20:00','COMPLETED',0),(15,'1.0.15.0','VisibilityOneDesktopPluginSetup_1.0.15.0.exe','2021-02-09 00:30:45',0,'21MB','Updated to token method desktop plugin connect',NULL,NULL,'COMPLETED',0),(16,'1.0.16.0','VisibilityOneDesktopPluginSetup_1.0.16.0.exe','2021-02-09 01:11:58',0,'21MB','Modified file name token parsing',NULL,NULL,'COMPLETED',0),(17,'1.0.17.0','VisibilityOneDesktopPluginSetup_1.0.17.0.exe','2021-02-10 03:30:57',0,'21MB','cpu info fix; mem_v1plugin and cpu_v1plugin rename',NULL,NULL,'COMPLETED',0),(18,'1.0.18.0','VisibilityOneDesktopPluginSetup_1.0.18.0.exe','2021-02-10 05:00:09',0,'21MB','modified cpu plugin usage calculation',NULL,'2021-02-10 06:00:00','COMPLETED',0),(19,'1.0.19.0','VisibilityOneDesktopPluginSetup_1.0.19.0.exe','2021-03-24 23:33:10',0,'22MB','Fix for cpu info',NULL,NULL,'COMPLETED',0),(20,'1.0.20.0','VisibilityOneDesktopPluginSetup_1.0.20.0.exe','2021-03-30 06:59:57',0,'22MB','Added teams support',NULL,'2021-03-30 07:53:00','COMPLETED',0),(21,'1.0.21.0','VisibilityOneDesktopPluginSetup_1.0.21.0.exe','2021-04-06 05:05:18',0,'22MB','Modified ping and new plugin data for teams',NULL,'2021-04-06 08:23:00','COMPLETED',0),(22,'1.0.22.0','VisibilityOneDesktopPluginSetup_1.0.22.0.exe','2021-04-09 07:24:58',0,'22MB','Call simulator fixes',NULL,'2021-04-09 07:44:00','COMPLETED',0),(23,'1.0.23.0','VisibilityOneDesktopPluginSetup_1.0.23.0.exe','2021-04-12 23:33:17',0,'22MB','Added device inuse values',NULL,'2021-04-12 23:57:00','COMPLETED',0),(24,'1.0.24.0','VisibilityOneDesktopPluginSetup_1.0.24.0.exe','2021-04-22 09:06:09',0,'22MB','Added silent installer and system default device inuse',NULL,NULL,'COMPLETED',0),(25,'1.0.25.0','VisibilityOneDesktopPluginSetup_1.0.25.0.exe','2021-05-03 22:22:26',0,'22MB','CPU usage optimization',NULL,'2021-05-30 02:14:00','COMPLETED',0),(26,'1.0.26.0','VisibilityOneDesktopPluginSetup_1.0.26.0.exe','2021-06-02 01:44:42',0,'22MB','Add teams call_status',NULL,NULL,'COMPLETED',0),(27,'1.0.27.0','VisibilityOneDesktopPluginSetup_1.0.27.0.exe','2021-06-11 08:32:30',0,'22MB','removed teams presence in data sending and added Save logs label in zip button',NULL,NULL,'COMPLETED',0),(28,'1.0.28.0','VisibilityOneDesktopPluginSetup_1.0.28.0.exe','2021-07-01 00:32:01',0,'22MB','Fix: in use values not showing on first teams/zoom call',NULL,'2021-07-01 02:48:00','COMPLETED',0),(30,'1.0.29.0','VisibilityOneDesktopPluginSetup_1.0.29.0.exe','2021-07-10 04:38:33',0,'22MB','Fix issue of pathping in specific environment for teams',NULL,'2021-07-10 06:00:33','COMPLETED',0),(31,'1.0.30.0','VisibilityOneDesktopPluginSetup_1.0.30.0.exe','2021-07-15 22:17:47',0,'22MB','Fix issue of get ip list in specific environment for zoom',NULL,'2022-02-13 09:46:00','COMPLETED',0),(32,'1.0.32.0','VisibilityOneDesktopPluginSetup_1.0.32.0.exe','2022-03-09 21:24:27',0,'22MB','Removed pathsolution calls in app simulator',NULL,NULL,'COMPLETED',0),(33,'1.0.33.0','VisibilityOneDesktopPluginSetup_1.0.33.0.exe','2022-03-14 10:21:17',0,'22MB','fixed empty device in windows 11',NULL,NULL,'COMPLETED',0),(34,'1.0.36.0','VisibilityOneDesktopPluginSetup_1.0.36.0.exe','2022-04-07 12:08:59',0,'22MB','Fixed disconnection issue due to cpu info error in WMI',NULL,NULL,'OPEN',0),(35,'2.0.0.1','VisibilityOneDesktopPluginSetup_2.0.0.1.exe','2022-04-07 12:08:59',1,'68MP','Add Taking Image from Cam',NULL,NULL,'OPEN',0);
/*!40000 ALTER TABLE `tbl_desktop_plugin_versions` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:22:48
