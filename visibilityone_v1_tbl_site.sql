-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_site`
--

DROP TABLE IF EXISTS `tbl_site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_site` (
  `site_id` int unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int unsigned DEFAULT NULL,
  `virtual` int DEFAULT '0',
  `parent_id` int unsigned DEFAULT NULL,
  `collector_id` int unsigned DEFAULT NULL,
  `site_name` varchar(127) DEFAULT NULL,
  `address1` varchar(127) DEFAULT NULL,
  `address2` varchar(127) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `active` int DEFAULT '1',
  `verified` int DEFAULT '0',
  `contact_id` int unsigned DEFAULT NULL,
  `paused` int DEFAULT '0',
  `status` int DEFAULT '-1',
  `alerts` int DEFAULT '1',
  `qos_paused` int DEFAULT '0',
  `localUTC` varchar(50) DEFAULT NULL,
  `wellness_check_time` time DEFAULT NULL,
  `schedule_interval` int unsigned DEFAULT NULL,
  PRIMARY KEY (`site_id`),
  KEY `company_idx` (`company_id`),
  KEY `report_idx` (`company_id`,`active`,`verified`),
  CONSTRAINT `company_site` FOREIGN KEY (`company_id`) REFERENCES `tbl_company` (`company_id`)
) ENGINE=InnoDB AUTO_INCREMENT=736 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_site`
--

LOCK TABLES `tbl_site` WRITE;
/*!40000 ALTER TABLE `tbl_site` DISABLE KEYS */;
INSERT INTO `tbl_site` VALUES (652,36,0,NULL,161,'NoCol','ad1','ad2','c','Central Visayas','174','123','2022-10-21 03:16:27',0,0,797,0,-1,1,0,NULL,NULL,NULL),(653,36,0,NULL,NULL,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2022-10-21 09:49:26',0,0,946,0,-1,1,0,NULL,NULL,NULL),(654,36,0,NULL,636,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2022-10-21 09:51:36',0,1,946,0,-1,1,0,NULL,NULL,NULL),(655,36,0,NULL,623,'Bicutan','Bicutan','Bicutan','Bicutan','Batangas','174','11111','2022-10-25 02:27:32',0,1,793,0,-1,1,0,NULL,NULL,NULL),(656,36,1,655,623,'Bicutan Virtual',NULL,NULL,NULL,NULL,NULL,NULL,'2022-10-26 02:27:44',0,1,793,0,-1,1,0,NULL,NULL,NULL),(657,36,0,NULL,638,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2022-10-29 07:50:27',0,1,946,0,-1,1,0,NULL,NULL,NULL),(658,36,0,NULL,639,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2022-10-29 10:36:20',0,1,946,0,-1,1,0,NULL,NULL,NULL),(659,36,0,NULL,623,'Bicutan','Bicutan','Bicutan','Bicutan','Batangas','174','1111','2022-11-05 02:35:39',0,1,793,0,-1,1,0,NULL,NULL,NULL),(660,36,0,NULL,643,'YasMac','I-10','','Islamabad','Federal Capital Area','167','46000','2022-12-14 10:28:25',0,1,946,0,-1,1,0,NULL,NULL,NULL),(661,36,0,NULL,644,'JT','Test site','','Somwhere','Florida','233','34285-3710','2022-12-29 23:39:07',0,1,938,0,-1,1,0,NULL,NULL,NULL),(662,36,0,NULL,645,'YasMac','I-10','','Islamabad','Federal Capital Area','167','46000','2023-01-17 10:49:24',0,1,946,0,-1,1,0,NULL,NULL,NULL),(663,36,0,NULL,644,'JT','Test Rd','','Somewhere','Florida','233','34285-3710','2023-01-24 23:55:44',0,1,938,0,-1,1,0,NULL,NULL,NULL),(664,36,0,NULL,623,'Taguig Mindanao','Taguig','Taguig','Taguig','Muslim Mindanao','174','1632','2023-01-25 04:02:34',0,1,793,0,1,1,0,NULL,NULL,NULL),(665,36,0,NULL,644,'JT Home','Test','','Test','Florida','233','34285-3710','2023-01-25 19:22:34',0,1,938,0,-1,1,0,NULL,NULL,NULL),(666,36,0,NULL,644,'JT Home','5808 Breeach Rd','','Somewhere cool','Florida','233','34285-3710','2023-01-31 22:31:20',0,1,938,0,-1,1,0,NULL,NULL,NULL),(667,36,0,NULL,88,'Arizona','Arizona','Arizona','Arizona','Arizona','233','1632','2023-02-14 04:51:14',0,1,793,0,-1,1,0,NULL,NULL,NULL),(668,36,0,NULL,646,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-02-20 09:21:39',0,1,946,0,-1,1,0,NULL,NULL,NULL),(669,36,0,NULL,NULL,'ahmed','ahmed','ahmed','ahmed','Kukes','3','1632','2023-02-22 06:11:55',0,0,793,0,-1,1,0,NULL,NULL,NULL),(670,36,0,NULL,NULL,'YasTest','Islamabad','','Islamabad','Federal Capital Area','167','46000','2023-02-28 07:52:46',0,0,946,0,-1,1,0,NULL,NULL,NULL),(671,386,0,NULL,647,'dummysite','Taguig','Taguig','Taguig','Muslim Mindanao','174','1632','2023-01-25 04:02:34',1,1,793,0,1,1,0,NULL,NULL,NULL),(672,386,0,NULL,648,'dummysite1','Taguig','Taguig','Taguig','Muslim Mindanao','174','1632','2023-01-25 04:02:34',1,1,793,0,1,1,0,NULL,NULL,NULL),(677,441,0,NULL,653,'dummysite1','Taguig','Taguig','Taguig','Muslim Mindanao','174','1632','2023-01-25 04:02:34',1,1,793,0,-1,1,0,NULL,NULL,NULL),(678,442,0,NULL,654,'test1','test1','test1','hey','Kapisa','1','16523','2023-03-10 11:38:04',1,1,925,0,-1,1,0,NULL,NULL,NULL),(679,442,1,678,654,'heheh',NULL,NULL,NULL,NULL,NULL,NULL,'2023-03-10 11:39:46',1,1,925,0,-1,1,0,NULL,NULL,NULL),(680,442,1,678,654,'hahasd',NULL,NULL,NULL,NULL,NULL,NULL,'2023-03-10 11:39:52',1,1,925,0,-1,1,0,NULL,NULL,NULL),(681,36,0,NULL,655,'Taguig Maharlika1','Taguig Maharlika','Taguig Maharlika','Taguig Maharlika','Batangas','174','1632','2023-03-14 08:23:10',0,1,793,0,-1,1,0,NULL,NULL,NULL),(682,36,0,NULL,656,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-03-16 10:54:41',0,1,946,0,-1,1,0,NULL,NULL,NULL),(690,36,0,NULL,665,'California','California','California','California','Alabama','233','11111','2023-04-05 04:24:55',0,1,793,0,-1,1,0,'UTC-07:00','22:07:10',NULL),(692,36,0,NULL,NULL,'Test','Kalalwala','Shiekhupura','Lahore','Federal Capital Area','167','52222','2023-04-05 12:10:31',0,0,106,0,-1,1,0,NULL,'01:30:10',NULL),(693,36,0,NULL,663,'Malik Ahsan22','Sanfransisco','Sanfransisco','Sanfransisco','Banteay Mean Chey','37','7777','2023-04-05 12:18:41',0,1,106,0,-1,1,0,NULL,'00:00:00',NULL),(694,36,0,NULL,656,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-04-07 06:35:14',0,1,946,0,-1,1,0,'UTC 05:00',NULL,NULL),(695,36,0,NULL,NULL,'Test Sibghat Ullah','Test Sibghat Ullah','Test Sibghat Ullah','Test Sibghat Ullah','Baluchistan','167','52222','2023-04-10 18:30:34',0,0,793,0,-1,1,0,NULL,NULL,NULL),(697,36,1,690,665,'Test Sibghat Ullah',NULL,NULL,NULL,NULL,NULL,NULL,'2023-04-13 06:02:30',0,1,793,0,-1,1,0,'UTC-07:00',NULL,NULL),(698,36,0,NULL,NULL,'Zamboanga','Zamboanga','Zamboanga','Zamboanga','Laxey','136','1111','2023-04-14 02:56:18',0,0,106,0,-1,1,0,NULL,NULL,NULL),(699,36,0,NULL,665,'Maharlika 041723','Maharlika ','Maharlika ','Maharlika ','Balkh','1','11111','2023-04-17 14:09:07',0,1,107,0,-1,1,0,NULL,NULL,NULL),(700,36,0,NULL,668,'Cebu','I-10','','Islamabad','Baluchistan','167','46000','2023-04-17 14:23:58',0,1,106,0,-1,1,0,'+08:00','05:30:10',1),(701,36,1,700,668,'Taguig',NULL,NULL,NULL,NULL,NULL,NULL,'2023-04-17 21:12:33',0,1,106,0,-1,1,0,'+08:00','05:30:10',1),(702,36,1,700,668,'Makati','Makati','Makati','Makati','Biskrah','4','11111','2023-04-17 21:12:42',0,1,443,0,-1,1,0,'+08:00','15:07:10',1),(703,36,0,NULL,669,'Tugbungan ','Tugbungan ','Tugbungan ','Tugbungan ','ash-Shalif','4','1111','2023-04-20 13:46:37',0,1,107,0,-1,1,0,'UTC-07:00',NULL,NULL),(704,36,1,693,663,'floor 1',NULL,NULL,NULL,NULL,NULL,NULL,'2023-06-01 16:28:07',0,1,443,0,-1,1,0,NULL,NULL,NULL),(705,36,0,NULL,669,'Testing for Visionpoint','test','','Test','Central','229','99663','2023-06-02 17:54:29',0,1,938,0,-1,1,0,'UTC-07:00',NULL,NULL),(706,36,0,NULL,NULL,'test1','test1','test1','zxczxc','Burj Bu Arririj','4','123123','2023-07-11 12:13:56',0,0,443,0,-1,1,0,NULL,'00:00:00',NULL),(707,36,0,NULL,656,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-07-14 04:17:27',0,1,946,0,-1,1,0,'+08:00','00:00:00',NULL),(708,36,0,NULL,672,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-07-14 04:47:19',0,1,946,0,-1,1,0,'+08:00','00:00:00',NULL),(709,36,0,NULL,674,'cebu ','cebu','cebu ','cebu ','Ghalizan','4','11111','2023-07-14 05:01:17',0,1,793,0,-1,1,0,'+08:00','00:00:00',NULL),(710,36,0,NULL,672,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-07-14 05:01:53',0,1,946,0,-1,1,0,'+08:00','00:00:00',NULL),(711,36,0,NULL,672,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-07-14 05:17:02',0,1,946,0,-1,1,0,'+08:00','00:00:00',NULL),(712,36,0,NULL,674,'cebu','cebu','cebu','cebu','Lagman','1','111111','2023-07-14 05:20:19',0,1,793,0,-1,1,0,'+08:00','00:00:00',NULL),(713,36,0,NULL,672,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-07-14 05:55:42',0,1,946,0,-1,1,0,'+08:00','00:00:00',NULL),(714,36,0,NULL,674,'Cebu New','Cebu New','Cebu New','Cebu New','Badakhshan','1','11111','2023-07-14 05:56:11',0,1,107,0,-1,1,0,'+08:00','00:00:00',NULL),(715,36,0,NULL,674,'Boalan','Boalan','Boalan','Boalan','Adrar','4','1632','2023-07-18 13:17:59',0,1,107,0,-1,1,0,'+08:00','00:00:00',NULL),(716,36,0,NULL,674,'Tugbungan','Tugbungan','Tugbungan','Tugbungan','Lagman','1','11111','2023-07-22 03:34:15',0,1,107,0,-1,1,0,'+08:00','11:40:10',NULL),(717,36,0,NULL,675,'Elizabith Test PC','I-10','','Islamabad','Federal Capital Area','167','46000','2023-07-22 05:27:10',0,1,946,0,1,1,0,'-07:00','00:00:00',NULL),(718,36,0,NULL,672,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-07-28 16:30:24',0,1,946,0,-1,1,0,' 05:00','00:00:00',NULL),(719,36,0,NULL,NULL,'ali','ali','ali','ali','Kukes','3','1111','2023-08-02 08:42:17',0,0,1013,0,-1,1,0,NULL,'00:00:00',NULL),(720,36,0,NULL,674,'Taguig','Taguig','Taguig','Taguig','Muslim Mindanao','174','11111','2023-08-04 14:27:10',0,1,793,0,-1,1,0,'-07:00','00:00:00',NULL),(721,36,0,NULL,676,'Elizabith Test PC','I-10','','Islamabad','Federal Capital Area','167','46000','2023-08-05 04:22:59',1,1,946,0,-1,1,0,'-07:00','00:00:00',NULL),(722,36,0,NULL,677,'Fahad Testing','123 East Street','Testing','Testing','Punjab','167','39500','2023-08-08 05:17:18',1,1,1011,0,-1,1,0,'-07:00','00:00:00',NULL),(723,36,0,NULL,678,'Windows16','I-10','','Islamabad','Federal Capital Area','167','46000','2023-08-08 17:04:59',1,1,946,0,-1,1,0,' 00:00','00:00:00',NULL),(724,36,0,NULL,NULL,'Fahad Dev Testing','test','test','Lahore','Punjab','167','39500','2023-08-10 06:54:30',1,0,1011,0,-1,1,0,NULL,'00:00:00',NULL),(725,36,0,NULL,679,'Windows 2019 Testing','description','description','Lahore','Baluchistan','167','39500','2023-08-10 13:17:14',1,1,1011,0,-1,1,0,'-07:00','00:00:00',NULL),(726,36,0,NULL,676,'Elizabeth new pc','Elizabeth new pc','Elizabeth new pc','Elizabeth new pc','Batangas','174','1632','2023-08-28 20:34:34',0,1,107,0,-1,1,0,'-07:00','06:10:10',NULL),(727,36,0,NULL,NULL,'test create','test create','test create','test create','Kukes','3','11111','2023-08-28 21:26:34',0,0,1013,0,-1,1,0,NULL,'01:30:10',NULL),(728,36,0,NULL,676,'Elizabet Manila','Elizabet Manila','Elizabet Manila','Elizabet Manila','Batangas','174','1632','2023-08-28 22:55:18',0,1,793,0,-1,1,0,'-07:00','16:00:00',NULL),(730,36,1,728,676,'liz v','liz v','liz v','liz v','Badakhshan','1','1632','2023-09-02 05:20:47',0,1,443,0,-1,1,0,'-07:00','05:30:00',NULL),(731,36,0,NULL,NULL,'test wellness','test wellness','test wellness','test wellness','-','2','16532','2023-09-02 05:45:14',0,0,1013,0,-1,1,0,NULL,'01:30:00',NULL),(732,36,0,NULL,676,'Liz tugbunganx','Liz tugbunganx','Liz tugbunganx','Liz tugbunganx','Luzon','174','1632','2023-09-03 03:30:43',1,1,107,0,-1,1,0,'-07:00','01:30:00',NULL),(733,36,1,732,676,'Liz Virtual','Liz Virtual','','Liz Virtual','Amazonas','173','16342','2023-09-03 03:57:00',1,1,1014,0,-1,1,0,'-07:00','01:30:00',NULL),(734,36,0,NULL,680,'Kamran_laptop','Islamabad','','Islamabad','Federal Capital Area','167','44000','2023-09-08 11:20:25',1,1,946,0,-1,1,0,' 05:00','00:00:00',NULL),(735,36,0,NULL,672,'YasPakistan','I-10','','Islamabad','Federal Capital Area','167','46000','2023-09-08 14:10:28',1,1,946,0,-1,1,0,' 05:00','00:00:00',NULL);
/*!40000 ALTER TABLE `tbl_site` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:58
