-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_documents`
--

DROP TABLE IF EXISTS `tbl_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_documents` (
  `document_id` bigint NOT NULL AUTO_INCREMENT,
  `company_id` bigint DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_type` varchar(20) DEFAULT NULL,
  `file_size` int DEFAULT NULL,
  `file_path` text,
  `description` text,
  `owner_user_id` bigint DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `active` int DEFAULT NULL,
  `quadrant_group` enum('VIDEO','AUDIO','CLOUD','DOMOTZ_IOT') DEFAULT NULL,
  `device_id` bigint DEFAULT NULL,
  `shared_by_user_id` bigint DEFAULT NULL,
  `share_expiration` datetime DEFAULT NULL,
  `shared_date` datetime DEFAULT NULL,
  PRIMARY KEY (`document_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_documents`
--

LOCK TABLES `tbl_documents` WRITE;
/*!40000 ALTER TABLE `tbl_documents` DISABLE KEYS */;
INSERT INTO `tbl_documents` VALUES (1,36,'1693695728845__sendFile.tgz','text/plain',5588636,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7042/1693695728845__sendFile.tgz','1693695728845__sendFile.tgz',9910,'2023-09-02 23:02:09','2023-09-02 23:02:09',1,'VIDEO',7042,9910,'2023-09-02 23:02:09','2023-09-02 23:02:09'),(2,36,'1693695757563__sendFile.tgz','text/plain',5596728,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7042/1693695757563__sendFile.tgz','1693695757563__sendFile.tgz',9910,'2023-09-02 23:02:38','2023-09-02 23:02:38',1,'VIDEO',7042,9910,'2023-09-02 23:02:38','2023-09-02 23:02:38'),(5,36,'1693837032841__sendFile.tgz','text/plain',5316560,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1693837032841__sendFile.tgz','1693837032841__sendFile.tgz',9910,'2023-09-04 14:17:13','2023-09-04 14:17:13',1,'VIDEO',7046,9910,'2023-09-04 14:17:13','2023-09-04 14:17:13'),(6,36,'1693837063389__sendFile.tgz','text/plain',5324738,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1693837063389__sendFile.tgz','1693837063389__sendFile.tgz',9910,'2023-09-04 14:17:43','2023-09-04 14:17:43',1,'VIDEO',7046,9910,'2023-09-04 14:17:43','2023-09-04 14:17:43'),(7,36,'1694183510836__sendFile.tgz','text/plain',5345555,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694183510836__sendFile.tgz','1694183510836__sendFile.tgz',9910,'2023-09-08 14:31:52','2023-09-08 14:31:52',1,'VIDEO',7046,9910,'2023-09-08 14:31:52','2023-09-08 14:31:52'),(8,36,'1694183550208__sendFile.tgz','text/plain',5231816,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694183550208__sendFile.tgz','1694183550208__sendFile.tgz',9910,'2023-09-08 14:32:30','2023-09-08 14:32:30',1,'VIDEO',7046,9910,'2023-09-08 14:32:30','2023-09-08 14:32:30'),(9,36,'1694269911028__sendFile.tgz','text/plain',5609131,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694269911028__sendFile.tgz','1694269911028__sendFile.tgz',9910,'2023-09-09 14:31:51','2023-09-09 14:31:51',1,'VIDEO',7046,9910,'2023-09-09 14:31:51','2023-09-09 14:31:51'),(10,36,'1694269939546__sendFile.tgz','text/plain',5616457,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694269939546__sendFile.tgz','1694269939546__sendFile.tgz',9910,'2023-09-09 14:32:19','2023-09-09 14:32:19',1,'VIDEO',7046,9910,'2023-09-09 14:32:19','2023-09-09 14:32:19'),(11,36,'1694356304061__sendFile.tgz','text/plain',5548887,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694356304061__sendFile.tgz','1694356304061__sendFile.tgz',9910,'2023-09-10 14:31:45','2023-09-10 14:31:45',1,'VIDEO',7046,9910,'2023-09-10 14:31:45','2023-09-10 14:31:45'),(12,36,'1694356332326__sendFile.tgz','text/plain',5558730,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694356332326__sendFile.tgz','1694356332326__sendFile.tgz',9910,'2023-09-10 14:32:13','2023-09-10 14:32:13',1,'VIDEO',7046,9910,'2023-09-10 14:32:13','2023-09-10 14:32:13'),(13,36,'1694442713331__sendFile.tgz','text/plain',5454550,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694442713331__sendFile.tgz','1694442713331__sendFile.tgz',9910,'2023-09-11 14:31:53','2023-09-11 14:31:53',1,'VIDEO',7046,9910,'2023-09-11 14:31:53','2023-09-11 14:31:53'),(14,36,'1694442742620__sendFile.tgz','text/plain',5466343,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694442742620__sendFile.tgz','1694442742620__sendFile.tgz',9910,'2023-09-11 14:32:22','2023-09-11 14:32:22',1,'VIDEO',7046,9910,'2023-09-11 14:32:22','2023-09-11 14:32:22'),(15,36,'1694529111337__sendFile.tgz','text/plain',5283799,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694529111337__sendFile.tgz','1694529111337__sendFile.tgz',9910,'2023-09-12 14:31:52','2023-09-12 14:31:52',1,'VIDEO',7046,9910,'2023-09-12 14:31:52','2023-09-12 14:31:52'),(16,36,'1694529152935__sendFile.tgz','text/plain',5295441,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694529152935__sendFile.tgz','1694529152935__sendFile.tgz',9910,'2023-09-12 14:32:33','2023-09-12 18:39:02',1,'VIDEO',7046,NULL,NULL,NULL),(18,36,'dummy','pdf',13264,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36%2FVIDEO%2F7047%2Fdummy.pdf','studioX30',793,'2023-09-12 19:01:35','2023-09-12 19:01:35',1,'VIDEO',7047,NULL,NULL,NULL),(19,36,'1694615518946__sendFile.tgz','text/plain',5436328,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694615518946__sendFile.tgz','1694615518946__sendFile.tgz',9910,'2023-09-13 14:32:00','2023-09-13 14:32:00',1,'VIDEO',7046,9910,'2023-09-13 14:32:00','2023-09-13 14:32:00'),(20,36,'1694615549370__sendFile.tgz','text/plain',5443970,'https://devv2-document-center.s3.us-west-1.amazonaws.com/36/VIDEO/7046/1694615549370__sendFile.tgz','1694615549370__sendFile.tgz',9910,'2023-09-13 14:32:29','2023-09-13 14:32:29',1,'VIDEO',7046,9910,'2023-09-13 14:32:29','2023-09-13 14:32:29');
/*!40000 ALTER TABLE `tbl_documents` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:18
