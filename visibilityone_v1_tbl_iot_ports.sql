-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_iot_ports`
--

DROP TABLE IF EXISTS `tbl_iot_ports`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_iot_ports` (
  `iot_port_id` bigint NOT NULL AUTO_INCREMENT,
  `port_name` varchar(150) DEFAULT NULL,
  `port_id` bigint DEFAULT NULL,
  `iot_device_id` bigint DEFAULT NULL,
  `is_monitored` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `port_status` tinyint(1) NOT NULL DEFAULT '0',
  `port_name_label` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`iot_port_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_iot_ports`
--

LOCK TABLES `tbl_iot_ports` WRITE;
/*!40000 ALTER TABLE `tbl_iot_ports` DISABLE KEYS */;
INSERT INTO `tbl_iot_ports` VALUES (23,'domain',53,9,1,1,1,NULL),(24,'https',443,9,1,1,1,NULL),(25,'https',443,10,1,1,1,NULL),(26,'http-alt',8000,10,1,1,1,NULL),(27,'vcom-tunnel',8001,10,1,1,1,NULL),(28,'rxapi',10010,10,1,1,1,NULL),(29,'domain',53,11,1,1,1,NULL),(30,'https',443,11,1,1,1,NULL),(51,'ssh',22,20,1,1,1,NULL),(52,'telnet',23,20,1,1,1,NULL),(53,'priv_mail',24,20,1,1,1,NULL),(54,'http',80,20,1,1,1,NULL),(55,'https',443,20,1,1,1,NULL),(56,'h323q931',1720,20,1,1,1,NULL),(57,'commplex_link',5001,20,1,1,1,NULL),(58,'sip_tls',5061,20,1,1,1,NULL),(65,'domain',53,23,1,1,1,NULL),(66,'https',443,23,1,1,1,NULL);
/*!40000 ALTER TABLE `tbl_iot_ports` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:23:53
