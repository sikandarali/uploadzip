-- MySQL dump 10.13  Distrib 8.0.33, for macos13 (x86_64)
--
-- Host: db-dev01.cssyf1yye5g2.us-west-1.rds.amazonaws.com    Database: visibilityone_v1
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED=/*!80000 '+'*/ '';

--
-- Table structure for table `tbl_desktop_plugin_actions`
--

DROP TABLE IF EXISTS `tbl_desktop_plugin_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tbl_desktop_plugin_actions` (
  `action_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `desktop_plugin_id` int DEFAULT NULL,
  `action` varchar(45) DEFAULT NULL,
  `params` json DEFAULT NULL,
  `request_stamp` datetime DEFAULT NULL,
  `results` json DEFAULT NULL,
  `results_msssage` varchar(255) DEFAULT NULL,
  `status` enum('OPEN','PROCESSING','COMPLETE','ABORTED','FAILED','EXPIRED') CHARACTER SET big5 COLLATE big5_chinese_ci DEFAULT 'OPEN',
  `complete_stamp` datetime DEFAULT NULL,
  `user_id` int unsigned DEFAULT NULL,
  `db_sync` int DEFAULT '0',
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_desktop_plugin_actions`
--

LOCK TABLES `tbl_desktop_plugin_actions` WRITE;
/*!40000 ALTER TABLE `tbl_desktop_plugin_actions` DISABLE KEYS */;
INSERT INTO `tbl_desktop_plugin_actions` VALUES (2,46,'UpdateDesktopPlugin','{\"url\": \"https://dev-dash.visibility.one/download_desktop_plugin/VisibilityOneDesktopPluginSetup_Rw.exe\", \"version\": \"2.0.0.1\"}','2023-05-05 13:31:29','{\"status\": \"OK\", \"message\": \"Plugin is successfully updated\"}','Plugin is successfully updated','COMPLETE','2023-05-19 12:35:50',NULL,0),(3,31,'RemoteHealMicrophone','{\"id\": \"{0.0.1.00000000}.{8db056e7-c86f-4d7c-a2ce-aad3444f6448}\"}','2023-06-07 04:55:24','{\"status\": \"OK\", \"message\": \"Device successfully found.\", \"datetime\": \"2023-06-07T04:55:55.0511652Z\"}','Device successfully found.','COMPLETE','2023-06-07 04:55:55',106,0);
/*!40000 ALTER TABLE `tbl_desktop_plugin_actions` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-09-13 23:24:12
